function onclickLogin() {
xmlHttp = GetXmlHttpObject();
if (xmlHttp == null) {
alert("Trình duyệt của bạn không hỗ trợ HTTP Request");
return false;
}

var uid = document.getElementById("email");
var pwd = document.getElementById("password");
var btLogin = document.getElementById("submit");
var errms = document.getElementById("err-ms");

if (uid.value.trim() === "" || pwd.value.trim() === "") {
//
errms.setAttribute("class","alert alert-danger")
errms.innerHTML = "Nhập email và password!";
return false; // Ket thuc xu ly
} else {
//
//
var url = "./CheckLogin";
url = url + "?user-email=" + uid.value.trim() + "&user-password=" + pwd.value.trim() + "&r="
+ btLogin.value.trim();

xmlHttp.onreadystatechange = stateChanged;
xmlHttp.open("GET", url, true);
xmlHttp.send(null);
return false;
}
return false;
}

function delete_ChildNode() {
var parentNodes = document.getElementById("login-bar");
while (parentNodes.firstChild) {
parentNodes.removeChild(parentNodes.firstChild);
}
}

function stateChanged() {
	if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
		var showdata = xmlHttp.responseText;
		if (showdata.trim() === "0") {
		//
			var errms = document.getElementById("err-ms");
			errms.setAttribute("class","alert alert-danger")
			errms.innerHTML = "Mật khẩu hoặc tài khoản không đúng!";
		
		} else {
		//
			window.location="homepage";
		}
	}
}

function GetXmlHttpObject() {
var xmlHttp = null;
try {
xmlHttp = new XMLHttpRequest();
} catch (e) {
try {
xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
} catch (e) {
xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
}
}
return xmlHttp;
}

function deleteErrms(){
	var errms = document.getElementById("err-ms");
	errms.removeAttribute("class");
	errms.innerHTML = "";
}

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}
function onclickSignup() {
	var uid = document.getElementById("signup-email");
	var pwd = document.getElementById("signup-password");
	var rpwd = document.getElementById("signup-rePassword");
	//var pwd1= pwd.value.replace(" ","");
	var fname = document.getElementById("signup-fullName");
	var dob = document.getElementById("signup-dateOfBirth");
	var pnbr = document.getElementById("signup-phoneNumber");
	var add = document.getElementById("signup-address");
	var wpl =  document.getElementById("workingPlace");
	var career = document.getElementById("signup-career");
	var errms2 = document.getElementById("err-password");
	var errms3 = document.getElementById("err-rePassword");
	var errms1 = document.getElementById("err-email");
	var errms4 = document.getElementById("err-fullName");
	if (uid.value.trim() === ""){
		errms1.removeAttribute("class");
		errms1.setAttribute("class","alert alert-danger")
		errms1.innerHTML = "Vui lòng nhập email!";
		return false; // Ket thuc xu ly
	} else if(!validateEmail(uid.value)){
		errms1.removeAttribute("class");
		errms1.setAttribute("class","alert alert-danger")
		errms1.innerHTML = "Email không hợp lệ!";
		return false;
	}else if(pwd.value.trim() === ""){
		errms2.removeAttribute("class");
		errms2.setAttribute("class","alert alert-danger")
		errms2.innerHTML = "Vui lòng nhập mật khẩu!";
		return false;
	}else if(pwd.value.include(" ")){
		errms2.removeAttribute("class");
		errms2.setAttribute("class","alert alert-danger")
		errms2.innerHTML = "Mật khẩu không hợp lệ!";
		return false;
	}else if(rpwd.value.trim() !== pwd.value.trim()){
		errms3.removeAttribute("class");
		errms3.setAttribute("class","alert alert-danger")
		errms3.innerHTML = "Mật khẩu nhập lại không giống!";
		return false;
	}else if(fname.value.trim() === ""){
		errms4.removeAttribute("class");
		errms4.setAttribute("class","alert alert-danger")
		errms4.innerHTML = "Nhập tên hiển thị cho tài khoản!";
		return false;
	}
}

function hiddenEmailErr(){
	var errms1 = document.getElementById("err-email");
	errms1.removeAttribute("class");
	errms1.setAttribute("class","hidden");
}

function hiddenPassErr(){
	var errms1 = document.getElementById("err-password");
	errms1.removeAttribute("class");
	errms1.setAttribute("class","hidden");
}

function hiddenRePasswordErr(){
	var errms1 = document.getElementById("err-rePassword");
	errms1.removeAttribute("class");
	errms1.setAttribute("class","hidden");
}

function hiddenFullNameErr(){
	var errms1 = document.getElementById("err-fullName");
	errms1.removeAttribute("class");
	errms1.setAttribute("class","hidden");
}

-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 14, 2016 at 02:25 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `findingtutor`
--

-- --------------------------------------------------------

--
-- Table structure for table `advertisementtype`
--

CREATE TABLE IF NOT EXISTS `advertisementtype` (
`AdvertisementTypeID` int(20) NOT NULL,
  `AdvertisementTypeDetail` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `advertisementtype`
--

INSERT INTO `advertisementtype` (`AdvertisementTypeID`, `AdvertisementTypeDetail`) VALUES
(1, 'Hiển thị trong top 10 bài đăng hàng đầu trong 1 ngày'),
(2, 'Hiển thị trong top 10 bài đăng hàng đầu trong 1 ngày'),
(3, 'Hiển thị trong top 5 bài đăng hàng đầu trong 2 ngày');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
`PostID` int(20) NOT NULL,
  `Email` varchar(200) DEFAULT NULL,
  `PostTitle` varchar(100) DEFAULT NULL,
  `PostTypeID` int(20) DEFAULT NULL,
  `PostAreaID` int(20) DEFAULT NULL,
  `Date` date DEFAULT NULL,
  `PostDetail` varchar(500) DEFAULT NULL,
  `Codes` varchar(20) DEFAULT NULL,
  `IsClosed` tinyint(1) DEFAULT '0',
  `AdvertisementNumber` int(11) DEFAULT '-1',
  `Subjects` varchar(200) DEFAULT NULL,
  `TimeCanTeachID` int(5) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`PostID`, `Email`, `PostTitle`, `PostTypeID`, `PostAreaID`, `Date`, `PostDetail`, `Codes`, `IsClosed`, `AdvertisementNumber`, `Subjects`, `TimeCanTeachID`) VALUES
(3, 'quang@quang.com', 'Nhận dạy cấp 2 Anh văn', 1, 2, '2016-10-11', 'Dạy kèm tiếng Anh', NULL, 0, 11, 'Anh văn', 2),
(4, 'quang@quang.com', 'Nhận dạy học sinh lớp 12 luyện thi đại học', 1, 2, '2016-10-14', 'Gia sư 5 năm kinh nghiệm.\r\nÔn luyện và giúp đỡ hơn 100 sinh viên đậu vào các trường Đại học hàng đầu Việt Nam như Y Hà Nội, Y Sài Gòn, Bách khoa Hà Nội', NULL, 0, 11, 'Toán Hóa Sinh', 1),
(6, 'quang2@quang.com', 'Luyện thi TOEIC', 1, 1, '2016-10-14', 'Chứng chỉ TOEC 990, kinh nghiệm luyện đề TOEIC 4 năm và giúp đỡ sinh viên. Đảm bảo đầu ra 700+ nếu theo đủ khóa học. Không đạt không hoàn tiền', NULL, 0, 10, 'English', 2),
(7, 'quang2@quang.com', 'Luyện thi IELTS', 1, 1, '2016-10-14', 'IELTS 9.0. Du học Mỹ 4 năm và mong muốn giúp đỡ học sinh Việt Nam có trình độ tiếng Anh tốt', NULL, 0, 10, 'English', 1),
(8, 'hoai@gmail.com', 'Tìm Gv dạy giỏi', 2, 5, '2016-11-14', 'Tìm GV dạy Toán Lý Hoá ôn thi đại học', NULL, 0, -1, 'Toán Lý Hoá', 2),
(9, 'son@gmail.com', 'Gv dạy lập trình C cơ bản', 1, 5, '2016-11-14', 'Dạy lập trình C/C++', NULL, 0, -1, 'C++/C', 3),
(10, 'son@gmail.com', 'Tìm Gv ôn thi đại học', 2, 2, '2016-11-14', 'Tìm gia sư ôn thi đại học', NULL, 0, -1, 'Toán lý hoá', 1),
(11, 'son@gmail.com', 'Nhận dạy ôn thi đại học', 1, 5, '2016-11-14', ' ôn thi đại học các môn khối A', NULL, 0, -1, 'Toán lý hoá', 1),
(12, 'quang4@quang.com', 'Tìm gia sư uy tín', 2, 3, '2016-11-14', 'Tìm gia sư cho con học lớp 9', NULL, 0, -1, 'Toán Lý Hoá Anh', 3),
(13, 'phanxuantrinhpxt@gmail.com', 'Nhận dạy kèm tiếng Anh tại nhà', 1, 7, '2016-11-14', 'Nhận dạy kèm thi tiếng anh giao tiếp', NULL, 0, -1, 'Anh Văn', 2),
(14, 'quang4@quang.com', 'Tìm gia sư dạy văn sử địa', 2, 5, '2016-11-14', 'Tìm gia sư dạy văn sử địa phục vụ cho thi đại học', NULL, 0, -1, 'Văn, sử, địa', 2),
(15, 'phanxuantrinhpxt@gmail.com', 'Tìm người dạy kèm tiếng Anh TOEIC', 2, 5, '2016-11-14', 'Tìm người dạy kèm tiếng Anh TOEIC để dạt 450+', NULL, 0, -1, 'Tiếng Anh', 2),
(16, 'quang4@quang.com', 'Tìm người dạy học tại nhà', 2, 5, '2016-11-14', 'Tìm giáo viên dạy học sinh tiểu học', NULL, 0, -1, 'Toán, tiếng Việt', 1),
(17, 'phanxuantrinhpxt@gmail.com', 'Nhận dạy tin học văn phòng tại nhà.', 1, 4, '2016-11-14', 'Nhận dạy word, excel...', NULL, 0, -1, 'Tin học', 3),
(18, 'hoai@gmail.com', 'Tuyển gia sư ôn thi đại học khối A1', 2, 3, '2016-11-14', 'Tuyển gia sư dạy cá môn khối A1, giá cả thương lượng', NULL, 0, -1, 'Toán, lý, Anh', 3),
(19, 'quang2@quang.com', 'Dạy toán bồi dưỡng học sinh giỏi', 1, 1, '2016-11-14', 'Nhận dạy kèm toán phục vụ thi học sih giỏi các cấp', NULL, 0, -1, 'Toán', 3),
(20, 'trinh12345@gmail.com', 'Tìm gia sư dạy học tiếng Anh tại nhà', 2, 1, '2016-11-14', 'Tìm gia sư dạy học tiếng Anh nghe đọc viết cơ bản', NULL, 0, -1, 'Anh', 2),
(21, 'abcde@gmail.com', 'Nhận ôn thi cấp tốc cho thi đại học', 2, 3, '2016-11-14', 'Nhận ôn thi cấp tốc cho thi đại học khối A tại nhà, bảo đảm đậu.', NULL, 0, -1, 'Toán, Lý, Hoá', 1),
(22, 'student1@gmail.com', 'Tìm ngươi dạy toán học sinh giỏi', 2, 4, '2016-11-14', 'Tìm ngươi dạy toán học sinh giỏi đi thi cấp quôc gia.', NULL, 0, -1, 'Toán', 2),
(23, 'hoai@gmail.com', 'Nhận dạy tiếng nhật tại nhà', 1, NULL, '2016-11-14', 'Nhận dạy tiếng nhật N3 tại nhà', NULL, 0, -1, 'Tiếng Nhật', 2),
(24, 'quang3@quang.com', 'Nhận dạy tiếng Anh cho học sinh mất gốc', 1, 6, '2016-11-14', 'Nhận dạy tiếng Anh cho học sinh mất gốc, giá cả thương lượng và tuỳ  theo thời gian dạy', NULL, 0, -1, 'Anh', 1),
(25, 'son@gmail.com', 'Nhận dạy lập trình cơ bản cho người mới', 1, 7, '2016-11-15', 'Nhận dạy lập trình cơ bản cho người mới, với các ngôn ngữ phổ biến', NULL, 0, -1, 'Tin', 3);

-- --------------------------------------------------------

--
-- Table structure for table `postarea`
--

CREATE TABLE IF NOT EXISTS `postarea` (
`PostAreaID` int(20) NOT NULL,
  `PostAreaDetail` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `postarea`
--

INSERT INTO `postarea` (`PostAreaID`, `PostAreaDetail`) VALUES
(1, 'Liên Chiểu'),
(2, 'Thanh Khê'),
(3, 'Hải Châu'),
(4, 'Sơn Trà'),
(5, 'Ngũ Hành Sơn'),
(6, 'Cẩm Lệ'),
(7, 'Hòa Vang');

-- --------------------------------------------------------

--
-- Table structure for table `posttype`
--

CREATE TABLE IF NOT EXISTS `posttype` (
`PostTypeID` int(20) NOT NULL,
  `PostTypeDetail` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posttype`
--

INSERT INTO `posttype` (`PostTypeID`, `PostTypeDetail`) VALUES
(1, 'Gia sư'),
(2, 'Suất dạy');

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE IF NOT EXISTS `report` (
`ReportID` int(20) NOT NULL,
  `PostID` int(20) DEFAULT NULL,
  `Email` varchar(200) DEFAULT NULL,
  `ReportDescription` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
`RoleID` int(20) NOT NULL,
  `RoleDetail` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`RoleID`, `RoleDetail`) VALUES
(0, 'Admin'),
(1, 'User');

-- --------------------------------------------------------

--
-- Table structure for table `timecanteach`
--

CREATE TABLE IF NOT EXISTS `timecanteach` (
`TimeCanTeachID` int(5) NOT NULL,
  `TimeCanTeachDetail` varchar(5) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `timecanteach`
--

INSERT INTO `timecanteach` (`TimeCanTeachID`, `TimeCanTeachDetail`) VALUES
(1, 'Sáng'),
(2, 'Chiều'),
(3, 'Tối');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `Email` varchar(200) NOT NULL,
  `RoleID` int(20) DEFAULT NULL,
  `Password` varchar(50) NOT NULL,
  `FullName` varchar(100) NOT NULL,
  `DateOfBirth` date DEFAULT NULL,
  `Career` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `WorkingPlace` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `PhoneNumber` int(20) DEFAULT NULL,
  `Address` varchar(200) DEFAULT NULL,
  `AvatarFileName` varchar(100) DEFAULT NULL,
  `IsBanned` tinyint(1) DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`Email`, `RoleID`, `Password`, `FullName`, `DateOfBirth`, `Career`, `WorkingPlace`, `PhoneNumber`, `Address`, `AvatarFileName`, `IsBanned`, `IsDeleted`) VALUES
('abcde@gmail.com', 1, '123456', 'Trung', '2000-05-15', 'Học sinh', 'THPT ăn chơi', 123456789, 'Hoà Khánh', NULL, NULL, NULL),
('hoai@gmail.com', 1, '12345', 'Hoai', '1994-11-08', 'SV', 'BKDN', 917353617, 'Hoa Khanh', NULL, NULL, NULL),
('phanxuantrinhpxt@gmail.com', 1, '123456', 'Phan Trinh', '1994-10-09', 'Sv', 'DHBK', 123456789, 'Lien Chieu', NULL, NULL, NULL),
('quang2@quang.com', 1, '123456', 'Liger', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0),
('quang3@quang.com', 1, '123456', 'Biger', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0),
('quang4@quang.com', 1, '123456', 'Phan Xuan Trinh', '1996-02-16', 'Sinh viên', 'Đại học Bách khoa', 1666111777, '12T2', '', 1, 1),
('quang@quang.com', 0, '123456', 'Ngô Trường Phạm Quang', '1994-02-26', 'Student', 'Đại học Bách khoa', 1657987849, 'K69/15 Ngô Thì Nhậm', NULL, 0, 0),
('son@gmail.com', 1, '123456', 'Sơn', '1994-11-15', 'Sinh viên', 'BKDN', 986523716, 'Sơn trà', NULL, NULL, NULL),
('student1@gmail.com', 1, '123456', 'Long', '1999-01-04', 'Học sinh', 'THPT ABC', 917353617, 'Hoà Vang', NULL, NULL, NULL),
('trinh12345@gmail.com', 1, '123456', 'Học', '2001-06-03', 'Học sinh', 'THPT Võ Thị Sáu', 917353617, 'đà nẵng', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `advertisementtype`
--
ALTER TABLE `advertisementtype`
 ADD PRIMARY KEY (`AdvertisementTypeID`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
 ADD PRIMARY KEY (`PostID`), ADD KEY `Email` (`Email`), ADD KEY `PostTypeID` (`PostTypeID`), ADD KEY `PostAreaID` (`PostAreaID`), ADD KEY `AdvertisementTypeID` (`AdvertisementNumber`), ADD KEY `TimeCanTeachID` (`TimeCanTeachID`);

--
-- Indexes for table `postarea`
--
ALTER TABLE `postarea`
 ADD PRIMARY KEY (`PostAreaID`);

--
-- Indexes for table `posttype`
--
ALTER TABLE `posttype`
 ADD PRIMARY KEY (`PostTypeID`);

--
-- Indexes for table `report`
--
ALTER TABLE `report`
 ADD PRIMARY KEY (`ReportID`), ADD KEY `PostID` (`PostID`), ADD KEY `Email` (`Email`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
 ADD PRIMARY KEY (`RoleID`);

--
-- Indexes for table `timecanteach`
--
ALTER TABLE `timecanteach`
 ADD PRIMARY KEY (`TimeCanTeachID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`Email`), ADD KEY `RoleID` (`RoleID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `advertisementtype`
--
ALTER TABLE `advertisementtype`
MODIFY `AdvertisementTypeID` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
MODIFY `PostID` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `postarea`
--
ALTER TABLE `postarea`
MODIFY `PostAreaID` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `posttype`
--
ALTER TABLE `posttype`
MODIFY `PostTypeID` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `report`
--
ALTER TABLE `report`
MODIFY `ReportID` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
MODIFY `RoleID` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `timecanteach`
--
ALTER TABLE `timecanteach`
MODIFY `TimeCanTeachID` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `post`
--
ALTER TABLE `post`
ADD CONSTRAINT `post_ibfk_1` FOREIGN KEY (`Email`) REFERENCES `user` (`Email`),
ADD CONSTRAINT `post_ibfk_2` FOREIGN KEY (`PostTypeID`) REFERENCES `posttype` (`PostTypeID`),
ADD CONSTRAINT `post_ibfk_3` FOREIGN KEY (`PostAreaID`) REFERENCES `postarea` (`PostAreaID`),
ADD CONSTRAINT `post_ibfk_5` FOREIGN KEY (`TimeCanTeachID`) REFERENCES `timecanteach` (`TimeCanTeachID`);

--
-- Constraints for table `report`
--
ALTER TABLE `report`
ADD CONSTRAINT `report_ibfk_1` FOREIGN KEY (`PostID`) REFERENCES `post` (`PostID`),
ADD CONSTRAINT `report_ibfk_2` FOREIGN KEY (`Email`) REFERENCES `user` (`Email`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`RoleID`) REFERENCES `role` (`RoleID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<%@page import="java.util.ArrayList"%>
<%@page import="model.BEAN.User"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	User user = (User) session.getAttribute("user");
	String message = (String) request.getAttribute("message");
	ArrayList<User> listAllUsers = (ArrayList<User>) request.getAttribute("listAllUsers");
	String searchContent = (String) (request.getAttribute("searchContent") == null ? ""
			: request.getAttribute("searchContent"));
	String typeOfUserSelection = (String) (request.getAttribute("typeOfUserSelection") == null ? "-1"
			: request.getAttribute("typeOfUserSelection"));
	String isBannedSelection = (String) (request.getAttribute("isBannedSelection") == null ? "-1"
			: request.getAttribute("isBannedSelection"));
	String isDeletedSelection = (String) (request.getAttribute("isDeletedSelection") == null ? "-1"
			: request.getAttribute("isDeletedSelection"));
%>
<jsp:include page="_header.jsp"></jsp:include>
<%
	if (user == null) {
%>
<section class="main-body" style="margin-top: 40em;" id="main-content">
	<div class="container">
		<!--Main content-->
		<div class="main-content">
			<b style="color: red;">Bạn không có quyền truy cập vào trang này!</b>
		</div>
	</div>
</section>
<%
	} else {
%>
<!-- Main content -->
<section class="main-body" style="margin-top: 40em;" id="main-content">
	<div class="container">
		<!--Main content-->
		<div class="main-content">
			<!--content-->
			<div class="row">
				<h2 style="text-align: center;">Quản lý người dùng website</h2>
			</div>
			<hr>
			<form action="quanlitaikhoan" method="post">
				<input type="hidden" name="act" value="timkiem">
				<div class="row">
					<div class="col-sm-5">
						<input class="form-control" type="text" name="searchContent"
							id="searchContent" value="<%=searchContent%>"
							placeholder="Nhập vào đây để tìm kiếm...">
					</div>
					<div class="col-sm-2">
						<select class="form-control" name="typeOfUserSelection"
							id="typeOfUserSelection">
							<option value="-1">Tất cả loại tài khoản</option>
							<option value="0"
								<%=typeOfUserSelection.equals("0") ? "selected" : ""%>>Admin</option>
							<option value="1"
								<%=typeOfUserSelection.equals("1") ? "selected" : ""%>>User</option>
						</select>
					</div>
					<div class="col-sm-2">
						<select class="form-control" name="isBannedSelection"
							id="isBannedSelection">
							<option value="-1">Tất cả trạng thái</option>
							<option value="0"
								<%=isBannedSelection.equals("0") ? "selected" : ""%>>Đang
								hoạt động</option>
							<option value="1"
								<%=isBannedSelection.equals("1") ? "selected" : ""%>>Cấm
								hoạt động</option>
						</select>
					</div>
					<div class="col-sm-2">
						<select class="form-control" name="isDeletedSelection"
							id="isDeletedSelection">
							<option value="-1">Tất cả tài khoản</option>
							<option value="0"
								<%=isDeletedSelection.equals("0") ? "selected" : ""%>>Đang
								hoạt động</option>
							<option value="1"
								<%=isDeletedSelection.equals("1") ? "selected" : ""%>>Đã
								xóa</option>
						</select>
					</div>
					<div class="col-sm-1">
						<!-- <button class="btn btn-primary btn-block" id="searchBtn"
							type="submit">
							<span class="fa fa-search" aria-hidden="true"></span>
						</button> -->
						<button class="btn btn-primary btn-block" type="submit">
							<span class="fa fa-search" aria-hidden="true"></span>
						</button>
					</div>
				</div>
			</form>
			<br>
			<div class="table">
				<div class="caption"
					style="text-align: center; text-decoration: bold;">
					<%
						if (message != null) {
					%>
					<!-- <b style="color: red;"></b> -->
					<div class="alert alert-success col-sm-6 col-sm-offset-3"><b><%=message%></b></div>
					<%
						}
					%>
				</div>
				<div class="tr">
					<span class="td"><b>STT</b></span> <span class="td"><b>Email</b></span>
					<span class="td"><b>Họ tên</b></span> <span class="td"><b>Vai
							trò</b></span> <span class="td"><b>Trạng thái</b></span> <span class="td"><b>Đã
							xóa</b></span> <span class="td"><b>Hành động</b></span>
				</div>
				<%
					for (int i = 0; i < listAllUsers.size(); i++) {
				%>
				<form class="tr" action="quanlitaikhoan" method="post">
					<span class="td"><%=i + 1%></span> <span class="td"><%=listAllUsers.get(i).getEmail()%></span>
					<span class="td"><a
						href="taikhoancanhan?act=userInfo&email=<%=listAllUsers.get(i).getEmail()%>"><%=listAllUsers.get(i).getFullName()%></a></span>
					<input type="hidden" name="act" value="ThayDoiThongTin"> <input
						type="hidden" name="email"
						value="<%=listAllUsers.get(i).getEmail()%>"> <span
						class="td"><input name="roleID" type="checkbox"
						<%=listAllUsers.get(i).getRoleID() == 0 ? "checked" : ""%>
						value="0" data-toggle="toggle" data-onstyle="info" data-on="Admin"
						data-off="User"></span> <span class="td"><input
						name="isBanned" type="checkbox"
						<%=listAllUsers.get(i).isBanned() ? "checked" : ""%> value="1"
						data-toggle="toggle" data-onstyle="danger" data-on="Banned"
						data-off="Active"></span> <span class="td"><input
						name="isDeleted" type="checkbox"
						<%=listAllUsers.get(i).isDeleted() ? "checked" : ""%> value="1"
						data-toggle="toggle" data-onstyle="warning" data-on="Deleted"
						data-off="Active"></span> <span class="td"><button
							class="btn btn-success btn-block" type="submit">Cập nhật</button></span>
				</form>
				<%
					}
				%>
			</div>
		</div>
	</div>
</section>
<%
	}
%>

<jsp:include page="_footer.jsp"></jsp:include>
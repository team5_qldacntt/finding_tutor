<%@page import="model.BO.GetPostAreaBO"%>
<%@page import="model.BEAN.Post"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.BEAN.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	User user = (User) session.getAttribute("user");
	ArrayList<Post> listPost = (ArrayList<Post>) request.getAttribute("listPost");
	String typeOfListPost = (String) request.getAttribute("typeOfListPost");
	GetPostAreaBO getPostAreaBO = new GetPostAreaBO();
	if (typeOfListPost == null) {
		typeOfListPost = "Tất cả bài đăng cá nhân";
	}
	String message = (String) request.getAttribute("message");
%>
<jsp:include page="_header.jsp"></jsp:include>


<!-- Main content -->
<section class="main-body" id="main-content" style="margin-top: 40em;">
	<div class="container">
		<div class="col-md-6 col-md-offset-4">
			<h1>
				<strong><%=typeOfListPost%></strong>
			</h1>
		</div>

		<!--aside infor-->
		<aside class="col-md-3">
			<div class="profile">
				<img src="images/user.png">
				<p><%=user.getFullName()%></p>
				<ul>
					<li>Nghề nghiệp: <%=user.getCareer()%></li>
					<li>Công tác: <%=user.getWorkingPlace()%></li>
					<li>Ngày sinh: <%=user.getDateOfBirth()%></li>
					<li>Kinh nghiệm: 4 năm</li>
					<li>Chuyên môn: Toán</li>
				</ul>
			</div>
			<jsp:include page="_sidebar.jsp"></jsp:include>
		</aside>
		<!--end aside-->

		<!--Main content-->
		<article class="col-md-8">
			<div class="main-content">

				<!--search-->
				<div class="search-group col-md-12">
					<input type="text">
					<button class="fa fa-search" aria-hidden="true"></button>
				</div>


				<!--content-->
				<%
					if (message != null) {
				%>
				<div class="dang-bai-line col-md-12">
					<div class="alert alert-success"><%=message%></div>
				</div>
				<%
					}
				%>
				<%
					for (int i = 0; i < listPost.size(); i++) {
				%>
				<div class="form-infor">
					<div class="post-item-personal col-md-12">
						<h2>
							<a
								href="ViewPost?act=edit&viewPostId=<%=listPost.get(i).getPostID()%>"><%=listPost.get(i).getPostTitle()%>
								<%
									if (listPost.get(i).isClosed()) {
								%> <b style="color: red;">(Đã đóng)</b> <%
 	}
 %></a>
						</h2>
						<div class="extar-infor1">
							<p style="float: none; font-size: small; color: orange;">
								<b>Ngày đăng:</b>
								<%=listPost.get(i).getDate()%></p>
						</div>
						<p>
							<i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;<b>Khu
								vực:</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=getPostAreaBO.getPostAreaById(listPost.get(i).getPostAreaID())%></p>
						<p>
							<i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;&nbsp;<b>Môn
								dạy/học:</b> &nbsp;<%=listPost.get(i).getSubjects()%></p>
						<div class="icon">
							<a href=""
								onclick="deletePost(<%=listPost.get(i).getPostID()%>);"><i
								class="fa fa-trash-o" aria-hidden="true"></i> Xóa</a>
							<%
								if (!listPost.get(i).isClosed()) {
							%>
							<a
								href="ViewPost?act=closepost&id=<%=listPost.get(i).getPostID()%>"><i
								class="fa fa-times" aria-hidden="true"></i> Đóng</a>
							<%
								} else {
							%>
							<a
								href="ViewPost?act=openpost&id=<%=listPost.get(i).getPostID()%>"><i
								class="fa fa-pencil" aria-hidden="true"></i> Mở</a>
							<%
								}
							%>
						</div>
					</div>
				</div>
				<%
					}
				%>
			</div>
		</article>


	</div>
</section>
<script type="text/javascript">
	function deletePost(postID){
		var result = confirm ("Bạn thât sự muốn xóa tin này? Bạn sẽ không thể hoàn tác.");
		if (result == true){
			$.ajax({
				type : "post",
				url : "ViewPost", //this is my servlet
				data : "act=deletepost&id=" + postID,
				success : function(result) {
					alert(result);
					window.location.reload();
				}
			});
		} else {
			window.location.reload();
		}
	}
</script>

<jsp:include page="_footer.jsp"></jsp:include>
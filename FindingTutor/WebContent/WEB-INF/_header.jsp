<%@page import="model.BEAN.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	User user = (User) session.getAttribute("user");
%>
<!doctype html>
<html>
<head>
<!-- Basic Page Needs -->
<meta charset="utf-8">
<title>GiaSu</title>
<meta name="description" content="A Template by Gozha.net">
<meta name="keywords" content="HTML, CSS, JavaScript">
<meta name="author" content="Gozha.net">

<!-- Mobile Specific Metas-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta content="telephone=no" name="format-detection">

<!-- Fonts -->
<!-- Font awesome - icon font -->
<link
	href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css"
	rel="stylesheet">
<!-- Roboto -->
<link href='http://fonts.googleapis.com/css?family=Roboto:400,700'
	rel='stylesheet' type='text/css'>

<!-- Stylesheets -->
<!-- jQuery UI -->
<link
	href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"
	rel="stylesheet">

<!-- Mobile menu -->
<link href="css/gozha-nav.css" rel="stylesheet" />
<!-- Select -->
<link href="css/external/jquery.selectbox.css" rel="stylesheet" />
<!-- Swiper slider -->
<link href="css/external/idangerous.swiper.css" rel="stylesheet" />
<!-- Magnific-popup -->
<link href="css/external/magnific-popup.css" rel="stylesheet" />



<!-- Custom -->
<link href="css/style.css?v=1" rel="stylesheet" />

<!-- gia su -->
<link href="css/gia-su.css" rel="stylesheet" />
<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet" />
<!-- Toggle -->
<link
	href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css"
	rel="stylesheet">

<!-- Modernizr -->
<script src="js/external/modernizr.custom.js"></script>
<script type="text/javascript">
	function goBack() {
		window.history.back();
	}
</script>
<script src="js/processValidate.js"></script>
<script src="js/signupValidate.js"></script>
<style>
DIV.table {
	display: table;
}

FORM.tr, DIV.tr {
	display: table-row;
}

SPAN.td {
	display: table-cell;
}

DIV.caption {
	display: table-caption;
}

.table, .td {
	border: 2px solid black;
	border-collapse: collapse;
}

.th, .td {
	padding: 15px;
}

#button-cancel {
	background-color: white;
	color: black;
}

a:focus, a:hover {
	color: darkorange;
	text-decoration: none;
}
</style>
</head>

<body>
	<div class="wrapper">
		<!-- Banner -->
		<div class="banner-top">
			<img alt='top banner' src="images/image_tutor/banner.png">
		</div>

		<!-- Header section -->
		<header class="header-wrapper">
			<div class="container">
				<!-- Logo link-->
				<div id="header-bar-fixed" class="">
					<a href="homepage" class="logo"> <img alt='logo'
						src="images/image_tutor/logo_giasu.png">
					</a>

					<!-- Main website navigation-->
					<nav id="navigation-box">
						<!-- Toggle for mobile menu mode -->
						<a href="#" id="navigation-toggle"> <span class="menu-icon">
								<span class="icon-toggle" role="button"
								aria-label="Toggle Navigation"> <span class="lines"></span>
							</span>
						</span>
						</a>

						<!-- Link navigation -->
						<ul id="navigation">
							<li><span class="sub-nav-toggle plus"></span> <a
								href="homepage#main-content">Trang chủ</a></li>
							<%
								if (user != null) {
							%>
							<li><span class="sub-nav-toggle plus"></span> <a href="#">Chào
									mừng <%=user.getFullName()%></a>
								<ul>
									<li class="dir"><a href="taikhoancanhan#main-content">Thông
											tin cá nhân</a></li>
									<li class="dir"><a href="baidangcanhan#main-content">Bài
											đăng cá nhân</a>
										<ul>
											<li><a href="baidangcanhan?type=suatday#main-content">Suất
													dạy</a></li>
											<li><a href="baidangcanhan?type=giasu#main-content">Gia
													sư</a></li>
											<li><a href="baidangcanhan?type=report#main-content">Bài
													đăng bị cảnh cáo</a></li>
										</ul></li>
									<%
										if (user.getRoleID() == 0) {
									%>
									<li><a href="quanlitaikhoan#main-content">Quản lý</a></li>
									<%
										}
									%>
									<li><a href="Logout">Đăng xuất</a></li>
								</ul></li>
							<%
								} else {
							%>
							<li><a class="login-window" href="#">Đăng nhập</a></li>
							<%
								}
							%>

							<!-- class='login-window' -->
							<span class="sub-nav-toggle plus"></span>
							<%
								if (session.getAttribute("user") == null)
									out.print("<li><a href='#' data-toggle='modal' data-target='#myModal'>Đăng ký</a></li>");
							%>
							<%
								if (user != null) {
							%>
							<li><span class="sub-nav-toggle plus"></span> <a
								href="dangbai#main-content">Đăng tin</a></li>
							<%
								} else {
							%>
							<li><span class="sub-nav-toggle plus"></span> <a
								class="login-window" href="#">Đăng tin</a></li>
							<%
								}
							%>
						</ul>
					</nav>
				</div>
				<!--content banner-->
				<div class="container-banner">
					<div class="text-banner">
						<p>
							Gia sư <strong>chất lượng</strong>
						</p>
						<p>
							Suất dạy <strong>uy tín</strong>
						</p>
					</div>
					<div class="group-button">
						<a href="homepage?idFildter=1#main-content" class="btn-banner">Gia
							sư</a> <a href="homepage?idFildter=2#main-content" class="btn-banner">Suất
							dạy</a>
					</div>
				</div>
			</div>
		</header>
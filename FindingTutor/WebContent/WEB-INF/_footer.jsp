<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<footer class="footer-wrapper col-md-12">
	<section class="container">
		<div class="col-xs-4 col-md-3 footer-nav">
			<ul class="nav-link">
				<li><a href="#" class="nav-link__item">Ngô Trường Phạm
						Quang</a></li>
				<li><a href="movie-list-left.html" class="nav-link__item">Trương
						Thị Hoài</a></li>
				<li><a href="trailer.html" class="nav-link__item">Huỳnh Kim
						Chính</a></li>
				<li><a href="rates-left.html" class="nav-link__item">Phan
						Xuân Trình</a></li>
				<li><a href="rates-left.html" class="nav-link__item">Trần
						Anh Thắng</a></li>
			</ul>
		</div>
		<div class="col-xs-4 col-md-3 footer-nav">
			<ul class="nav-link">
				<li><a href="coming-soon.html" class="nav-link__item">Hotline:0511
						847 598</a></li>
				<li><a href="cinema-list.html" class="nav-link__item">Fax:
						0511 784268</a></li>
				<li><a href="offers.html" class="nav-link__item">Cầu nối
						giữa gia sư và phụ huynh, học sinh</a></li>
				<li><a href="offers.html" class="nav-link__item">Đăng tin
						miễn phí</a></li>
				<li><a href="offers.html" class="nav-link__item">5k - top
						bài đăng</a></li>
			</ul>
		</div>

		<div class="col-xs-12 col-md-6">
			<div class="footer-info">
				<p class="heading-special--small">
					GiaSu<br> <span class="title-edition">cầu nối tin cậy</span>
				</p>

				<div class="social">
					<a href='#' class="social__variant fa fa-facebook"></a> <a href='#'
						class="social__variant fa fa-twitter"></a> <a href='#'
						class="social__variant fa fa-vk"></a> <a href='#'
						class="social__variant fa fa-instagram"></a> <a href='#'
						class="social__variant fa fa-tumblr"></a> <a href='#'
						class="social__variant fa fa-pinterest"></a>
				</div>

				<div class="clearfix"></div>
				<p class="copy">&copy; GiaSu, 2016. Copyright by Son Tran - Team #5</p>
			</div>
		</div>
	</section>
</footer>
<!-- JavaScript-->
<!-- jQuery 1.9.1-->
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script>
	window.jQuery
			|| document
					.write('<script src="js/external/jquery-1.10.1.min.js"><\/script>')
</script>
<!-- Migrate -->
<script src="js/external/jquery-migrate-1.2.1.min.js"></script>
<!-- jQuery UI -->
<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<!-- Bootstrap 3-->
<script
	src="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
<!-- Toggle -->
<script
	src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<!-- Mobile menu -->
<script src="js/jquery.mobile.menu.js"></script>
<!-- Select -->
<script src="js/external/jquery.selectbox-0.2.min.js"></script>

<script src="js/custom.js"></script>

<script>
	$(document).ready(function() {
		// Handler for .ready() called.
		$('html, body').animate({
			scrollTop : $('#main-content').offset().top
		}, 'slow');
	});
</script>

</body>
</html>

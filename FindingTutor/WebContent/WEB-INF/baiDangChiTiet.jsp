<%@page import="model.BO.GetTimeCanTeachBO"%>
<%@page import="model.BO.GetPostAreaBO"%>
<%@page import="model.BEAN.User"%>
<%@page import="model.BO.UserBO"%>
<%@page import="model.BEAN.Post"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	request.setCharacterEncoding("utf-8");
	Post post = (Post) request.getAttribute("post");
	UserBO userBO = new UserBO();
	User user = userBO.viewUserInformation(post.getEmail());
	GetPostAreaBO getPostAreaBO = new GetPostAreaBO();
	GetTimeCanTeachBO timeCanTeachBO = new GetTimeCanTeachBO();
	String postArea = getPostAreaBO.getPostAreaById(post.getPostAreaID());
	String timeCanTeach = timeCanTeachBO.getTimeCanTeachById(post.getTimeCanTeachID());
%>

<jsp:include page="/WEB-INF/_header.jsp"></jsp:include>


<!-- Main content -->
<section class="main-body" style="margin-top: 40em;" id="main-content">
	<div class="container">
		<div class="col-md-6 col-md-offset-5">
			<h1>
				<strong>Chi tiết bài đăng</strong>
			</h1>
		</div>

		<!--aside infor-->
		<aside class="col-md-3">
			<div class="profile">
				<img src="images/user.png">
				<p><%=user.getFullName()%></p>
				<ul>
					<li>Nghề nghiệp: <%=user.getCareer()%></li>
					<li>Công tác: <%=user.getWorkingPlace()%></li>
					<li>Ngày sinh: <%=user.getDateOfBirth()%></li>
					<li>Kinh nghiệm: 4 năm</li>
					<li>Chuyên môn: Toán</li>
				</ul>
			</div>
			<jsp:include page="_sidebar.jsp"></jsp:include>
		</aside>
		<!--end aside-->

		<!--Main content-->
		<article class="col-md-8">
			<div class="main-content">

				<!--search-->

				<!--content-->
				<div class="form-infor">
					<div class="post-item-personal col-md-12">
						<h2>
							<a><%=post.getPostTitle()%></a>
						</h2>
						<div class="infor-nguoidang">
							<img src="images/user.png" height="50%">
							<p>
								Người đăng:
								<%=user.getFullName()%></p>
						</div>
						<div class="extar-infor">
							<p>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ngày đăng:
								<%=post.getDate()%></p>
							<p>&nbsp;</p>
						</div>
						<p>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=post.getPostDetail()%></p>
						<div class="location">
							<i class="fa fa-map-marker" aria-hidden="true"></i>
							<p>
								<strong>Khu vực giảng dạy:</strong>
								<%=postArea%></p>
						</div>
						<div class="time">
							<i class="fa fa-calendar" aria-hidden="true"></i>
							<p>
								<strong>Thời gian:</strong>
								<%=timeCanTeach%></p>
						</div>
						<div class="email">
							<i class="fa fa-phone" aria-hidden="true"></i>
							<p>
								<strong>Liên hệ:</strong> 0<%=user.getPhoneNumber()%></p>
						</div>
						<div class="email">
							<i class="fa fa-envelope" aria-hidden="true"></i>
							<p>
								<strong>Môn:</strong>
								<%=post.getSubjects()%></p>
						</div>
						<!-- <div class="icon">
							<a href="ViewPost?act=reportpost?id=<%=post.getPostID()%>"><i
								class="fa fa-flag" aria-hidden="true"></i> Report</a>
						</div> -->
						<div class="fb-comments"
							data-href="http://localhost:8080/FindingTutor/ViewPost?viewPostId=<%=post.getPostID()%>"
							data-numposts="5"></div>
						<div class="btn-group col-sm-offset-10">
							<button class="btn-block" type="button" id="button-cancel"
								onclick="goBack();">Quay lại</button>
						</div>

					</div>

				</div>
			</div>
		</article>


	</div>
</section>

<jsp:include page="_footer.jsp"></jsp:include>


<!-- open/close -->
<div class="overlay overlay-hugeinc" id="login-modal">

	<section class="container">

		<div class="col-sm-4 col-sm-offset-4">
			<button type="button" class="overlay-close">Close</button>
			<form id="login-form" class="login" method="post"
				novalidate="novalidate" action="CheckLogin"
				onsubmit="return onclickLogin()">
				<p class="login__title">
					sign in <br> <span class="login-edition">welcome to
						GiaSu</span>
				</p>

				<div class="field-wrap">
					<p id="err-ms"></p>
					<input type='email' id="email" placeholder='Email'
						name='user-email' class="login__input" onfocus="deleteErrms()">
					<input type='password' id=password placeholder='Password'
						name='user-password' class="login__input" onfocus="deleteErrms()">
				</div>

				<div class="login__control">
					<button type="submit" id="submit"
						class="btn btn-md btn--warning btn--wider"
						onclick="onclickLogin()">sign in</button>
					<a href="ForgotPassword" class="login__tracker form__tracker">Forgot
						password?</a>
				</div>
			</form>
		</div>

	</section>
</div>
<!-- Modal -->
<!--

//-->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-body">
				<form id="login-form" class="login" method="post"
					novalidate="novalidate" action="Register"
					onsubmit="return onclickSignup()">
					<p class="login__title">
						sign up <br> <span class="login-edition">welcome to
							GiaSu</span>
					</p>

					<div class="field-wrap">
						<p id='err-email' class="hidden">aaaaaa</p>
						<input type='email' placeholder='Email(*)' name='email'
							id="signup-email" class="login__input" required
							onfocus="hiddenEmailErr()">
						<p id='err-password' class="hidden">aaaaaa</p>
						<input type='password' id="signup-password"
							placeholder='Mật khẩu(*)' name='password' class="login__input"
							required onfocus="hiddenPassErr()">
						<p id='err-rePassword' class="hidden">aaaaaa</p>
						<input type='password' placeholder='Nhập lại mật khẩu (*)'
							name='rePassword' id='signup-rePassword' class="login__input"
							required onfocus="hiddenRePasswordErr()">
						<p id='err-fullName' class="hidden">aaaaaa</p>
						<input type="text" placeholder='Tên hiển thị (*)' name='fullName'
							id='signup-fullName' class="login__input" required
							onfocus="hiddenFullNameErr()"> <input type="number"
							placeholder='Số điện thoại' name='phoneNumber'
							id='signup-phoneNumber' class="login__input"> <input
							type="text" placeholder='Địa chỉ' name='address'
							id='signup-address' class="login__input"> <input
							type="date" placeholder='Ngày sinh' name='dateOfBirth'
							id='signup-dateOfBirth' class="login__input" value="2000-01-01">
						<input type="text" placeholder='Nghề nghiệp' name='career'
							id='signup-career' class="login__input"> <input
							type="text" placeholder='Nơi làm việc' name='workingPlace'
							id='workingPlace' class="login__input">
					</div>

					<div class="login__control">
						<button type="submit" id="signup-submit"
							class="btn btn-md btn--warning btn--wider">Đăng ký</button>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<script>
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id))
			return;
		js = d.createElement(s);
		js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=1801334573469495";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>
<!-- JavaScript-->
<!-- jQuery 1.9.1-->
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script>
	window.jQuery
			|| document
					.write('<script src="js/external/jquery-1.10.1.min.js"><\/script>')
</script>
<!-- Migrate -->
<script src="js/external/jquery-migrate-1.2.1.min.js"></script>
<!-- jQuery UI -->
<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<!-- Bootstrap 3-->
<script
	src="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>

<!-- Mobile menu -->
<script src="js/jquery.mobile.menu.js"></script>
<!-- Select -->
<script src="js/external/jquery.selectbox-0.2.min.js"></script>

<script src="js/custom.js"></script>

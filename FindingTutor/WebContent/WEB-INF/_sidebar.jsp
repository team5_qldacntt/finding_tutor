<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="lop-dao-tao">
	<div class="title-bg">Khu vực</div>
	<ul class="item-bg">
		<li><a href="sidebarsearch?act=district&value=1">Liên Chiểu</a></li>
		<li><a href="sidebarsearch?act=district&value=2">Thanh Khê</a></li>
		<li><a href="sidebarsearch?act=district&value=3">Hải Châu</a></li>
		<li><a href="sidebarsearch?act=district&value=4">Sơn Trà</a></li>
		<li><a href="sidebarsearch?act=district&value=5">Ngũ Hành Sơn</a></li>
		<li><a href="sidebarsearch?act=district&value=6">Cẩm Lệ</a></li>
		<li><a href="sidebarsearch?act=district&value=7">Hòa Vang</a></li>
	</ul>
</div>
<div class="mon-hoc">
	<div class="title-bg">Môn học</div>
	<ul class="item-bg">
		<li><a href="sidebarsearch?act=subject&value=Luyen thi dai hoc khoi A">Luyện thi đại học khối A</a></li>
		<li><a href="sidebarsearch?act=subject&value=Toan">Toán</a></li>
		<li><a href="sidebarsearch?act=subject&value=Toan ly hoa cap 3">Toán lý hóa cấp 3</a></li>
		<li><a href="sidebarsearch?act=subject&value=English">Anh văn lớp 10</a></li>
	</ul>
</div>
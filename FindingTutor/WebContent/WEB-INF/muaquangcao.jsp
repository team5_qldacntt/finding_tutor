<%@page import="model.BEAN.UserAds"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<% ArrayList<UserAds> listUserAds = (ArrayList) request.getAttribute("listUserAds"); 
	String message = (String) request.getAttribute("message");
	%>
<jsp:include page="_header.jsp"></jsp:include>
        
        
        <!-- Main content -->
        <section class="main-body" style="margin-top:40em;" id="main-content">
            <div class="container">
                <div class="col-md-6 col-md-offset-6">
                    <h1><strong>Chọn gói quảng cáo</strong></h1>
                </div>

        <!--aside infor-->
                <aside class="col-md-3">
                    <div class="profile">
                        <img src="images/user.png">
                        <p>Username</p>
                        <ul>
                            <li>Nghề nghiệp: Sinh viên</li>
                            <li>Công tác: ĐH  Bách Khoa ĐN</li>
                            <li>Ngày sinh: 12/10/1996</li>
                            <li>Kinh nghiệm: 4 năm</li>
                            <li>Chuyên môn: Toán</li>
                        </ul>
                    </div>
                    <div class="lop-dao-tao">
                        <div class="title-bg">Lớp đào tạo</div>
                        <ul class="item-bg">
                            <li><a href="#">Cap 1</a></li>
                            <li><a href="#">Cap 2</a></li>
                            <li><a href="#">Cap 3</a></li>
                            <li><a href="#">Luyện thi Đại học</a></li>
                            <li><a href="#">Luyện thi Toeic</a></li>
                        </ul>
                    </div>
                    <div class="mon-hoc">
                        <div class="title-bg">Môn học</div>
                        <ul class="item-bg">
                            <li><a href="#">Luyện thi đại học khối A</a></li>
                            <li><a href="#">Toán lý hóa cấp 2</a></li>
                            <li><a href="#">Toán lý hóa cấp 3</a></li>
                            <li><a href="#">Anh văn lớp 10</a></li>
                        </ul>
                    </div>
                </aside>
        <!--end aside-->
                
        <!--Main content-->        
                <article class="col-md-8">
                    <div class="main-content col-md-12">
                        <%if(message!=null) {
                        	%>
                        	<p><%=message %></p>
                        <%}%>
                        <!--content goi quang cao-->
                        <%if(listUserAds != null) {
                        	for(int i=0; i<listUserAds.size(); i++) {%>
                        		<div class="goi-ads col-md-12">
                            <div class="col-md-6 package">
                                <img src="images/user.png">
                                <div class="text-ads">
                                <!-- <%=listUserAds.get(i).getAdvertisementType().getAdvertisementTypeDetail() %> -->
                                    <h4><%=listUserAds.get(i).getAdvertisementType().getAdvertisementTypeDetail() %></h4>
                                    <p><%=(i+1)*10000 %></p>
                                    <p>Ưu đãi</p>
                                </div>
                            </div>    
                            <div class="col-md-2 so-luong">
                                <p>Số lượng</p>
                                <p><%=listUserAds.get(i).getQuantity() %></p>
                            </div>
                            <div class="col-md-1 col-md-offset-2 btn-mua">
                                <a href="buyAds?AdsID=<%=listUserAds.get(i).getAdvertisementType().getAdvertisementTypeID()%>&AdsDetail=<%=listUserAds.get(i).getAdvertisementType().getAdvertisementTypeDetail() %>&thaoTac=mua">Mua</a>

                            </div>
                        </div>
                        	<%}
                        	%>
                       <% }%>
                        
                    </div>
                </article>
                
                
            </div>
        </section>
        
        <footer class="footer-wrapper col-md-12">
            <section class="container">
                <div class="col-xs-4 col-md-3 footer-nav">
                    <ul class="nav-link">
                        <li><a href="#" class="nav-link__item">Ngô Trường Phạm Quang</a></li>
                        <li><a href="movie-list-left.html" class="nav-link__item">Trương Thị Hoài</a></li>
                        <li><a href="trailer.html" class="nav-link__item">Huỳnh Kim Chính</a></li>
                        <li><a href="rates-left.html" class="nav-link__item">Phan Xuân Trình</a></li>
                        <li><a href="rates-left.html" class="nav-link__item">Trần Anh Thắng</a></li>
                    </ul>
                </div>
                <div class="col-xs-4 col-md-3 footer-nav">
                    <ul class="nav-link">
                        <li><a href="coming-soon.html" class="nav-link__item">Hotline:0511 847 598</a></li>
                        <li><a href="cinema-list.html" class="nav-link__item">Fax: 0511 784268</a></li>
                        <li><a href="offers.html" class="nav-link__item">Cầu nối giữa gia sư và phụ huynh, học sinh</a></li>
                        <li><a href="offers.html" class="nav-link__item">Đăng tin miễn phí</a></li>
                        <li><a href="offers.html" class="nav-link__item">5k - top bài đăng</a></li>
                    </ul>
                </div>
                
                <div class="col-xs-12 col-md-6">
                    <div class="footer-info">
                        <p class="heading-special--small">GiaSu<br><span class="title-edition">cầu nối tin cậy</span></p>

                        <div class="social">
                            <a href='#' class="social__variant fa fa-facebook"></a>
                            <a href='#' class="social__variant fa fa-twitter"></a>
                            <a href='#' class="social__variant fa fa-vk"></a>
                            <a href='#' class="social__variant fa fa-instagram"></a>
                            <a href='#' class="social__variant fa fa-tumblr"></a>
                            <a href='#' class="social__variant fa fa-pinterest"></a>
                        </div>
                        
                        <div class="clearfix"></div>
                        <p class="copy">&copy; GiaSu, 2016. Copyright by ...</p>
                    </div>
                </div>
            </section>
        </footer>
    </div>

    <!-- open/close -->
        <div class="overlay overlay-hugeinc">
            
            <section class="container">

                <div class="col-sm-4 col-sm-offset-4">
                    <button type="button" class="overlay-close">Close</button>
                    <form id="login-form" class="login" method='get' novalidate=''>
                        <p class="login__title">sign in <br><span class="login-edition">welcome to GiaSu</span></p>
                        
                        <div class="field-wrap">
                        <input type='email' placeholder='Email' name='user-email' class="login__input">
                        <input type='password' placeholder='Password' name='user-password' class="login__input">

                        <input type='checkbox' id='#informed' class='login__check styled'>
                         </div>
                        
                        <div class="login__control">
                            <button type='submit' class="btn btn-md btn--warning btn--wider">sign in</button>
                            <a href="#" class="login__tracker form__tracker">Forgot password?</a>
                        </div>
                    </form>
                </div>

            </section>
        </div>
    

	<!-- JavaScript-->
        <!-- jQuery 1.9.1--> 
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/external/jquery-1.10.1.min.js"><\/script>')</script>
        <!-- Migrate --> 
        <script src="js/external/jquery-migrate-1.2.1.min.js"></script>
        <!-- jQuery UI -->
        <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <!-- Bootstrap 3--> 
        <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>

        <!-- Mobile menu -->
        <script src="js/jquery.mobile.menu.js"></script>
         <!-- Select -->
        <script src="js/external/jquery.selectbox-0.2.min.js"></script>

        <script src="js/custom.js"></script>

</body>
</html>

<%@page import="model.BEAN.User"%>
<%@page import="model.BO.GetPostTypeBO"%>
<%@page import="model.BO.GetPostAreaBO"%>
<%@page import="model.BO.GetTimeCanTeachBO"%>
<%@page import="model.BEAN.Post"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
String message=(String)request.getAttribute("emailNotFound");
%>
<jsp:include page="/WEB-INF/_header.jsp"></jsp:include>


<!-- Main content -->
<section class="main-body" id="main-content" style="margin-top: 40em;">
	<div class="container">

		<!--aside infor-->
		<aside class="col-md-3">
			<jsp:include page="/WEB-INF/_sidebar.jsp"></jsp:include>
		</aside>
		<!--end aside-->

		<!--Main content-->
		<article class="col-md-8">
			<div class="main-content">

				<!--search-->
				<div class="search-group col-md-12">
					<form action="homepage" class="row">
						<input type="text" style="color: black;" name="searchKey">
						<button class="fa fa-search" aria-hidden="true" type="submit"></button>
					</form>
					<!-- Search option
					<div class="search-item" class="row">
						<form action="" class="col-md-6">
							<select name="subject">
								<option value="All">Môn học</option>
								<option value="Toan">Toán</option>
								<option value="Ly">Lý</option>
								<option value="Hoa">Hóa</option>
							</select>
						</form>
						<form action="" class="col-md-6">
							<select name="district">
								<option value="All">Quận</option>
								<option value="1">Liên Chiểu</option>
								<option value="2">Hải Châu</option>
								<option value="3">Thanh Khê</option>
								<option value="4">Sơn Trà</option>
								<option value="5">Ngũ Hành Sơn</option>
								<option value="6">Hòa Vang</option>
							</select>
						</form>
					</div>
					 -->
				</div>
			</div>
			<div class="col-md-8 col-md-offset-2">
			<br>
			<form action="ForgotPassword" method="POST" role="form">
				<legend >Lấy lại mật khẩu</legend>
			
				<div class="form-group">
				<%if(message!=null){ %>
					<div class="alert alert-danger " role="alert"><%=message %></div>
				<%}if(request.getAttribute("success")==null){ %>
					<label for="">Email</label>
					<input type="email" class="form-control" id="email" name="email" placeholder="Nhập email" required>
					<button type="submit" class="btn btn-primary">Lấy mật khẩu</button>
					<%}else{ %>
					<p><%=request.getAttribute("success") %></p>
					<%} %>
				</div>
			
				
			</form>
			</div>
		</article>


	</div>
</section>

<jsp:include page="/WEB-INF/_footer.jsp"></jsp:include>


<!-- open/close -->
<div class="overlay overlay-hugeinc" id="login-modal">

	<section class="container">

		<div class="col-sm-4 col-sm-offset-4">
			<button type="button" class="overlay-close">Close</button>
			<form id="login-form" class="login" method="post"
				novalidate="novalidate" action="CheckLogin" onsubmit="return onclickLogin()">
				<p class="login__title">
					sign in <br> <span class="login-edition">welcome to
						GiaSu</span>
				</p>

				<div class="field-wrap">		
					<p id="err-ms"></p>
					<input type='email' id="email" placeholder='Email' name='user-email'
						class="login__input" onfocus="deleteErrms()"> 
					<input type='password' id=password placeholder='Password' name='user-password' class="login__input" onfocus="deleteErrms()">
				</div>

				<div class="login__control">
					<button type="submit" id="submit" class="btn btn-md btn--warning btn--wider" onclick="onclickLogin()">sign
						in</button>
					<a href="ForgotPassword" class="login__tracker form__tracker">Forgot
						password?</a>
				</div>
			</form>
		</div>

	</section>
</div>
<!-- Modal -->
<!--

//-->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-body">
				<form id="login-form" class="login" method="post"
					novalidate="novalidate" action="Register" onsubmit="return onclickSignup()">
					<p class="login__title">
						sign up <br> <span class="login-edition">welcome to
							GiaSu</span>
					</p>

					<div class="field-wrap">
					<p id='err-email' class="hidden">aaaaaa</p>
						<input type='email' placeholder='Email(*)' name='email'
							id="signup-email" class="login__input" required onfocus="hiddenEmailErr()"> 
					<p id='err-password'class="hidden">aaaaaa</p><input
							type='password' id="signup-password" placeholder='Mật khẩu(*)' name='password'
							class="login__input" required onfocus="hiddenPassErr()"> 
					<p id='err-rePassword'class="hidden">aaaaaa</p><input
							type='password' placeholder='Nhập lại mật khẩu (*)' name='rePassword' id='signup-rePassword'
							class="login__input" required onfocus="hiddenRePasswordErr()"> 
					<p id='err-fullName'class="hidden">aaaaaa</p><input
							type="text" placeholder='Tên hiển thị (*)' name='fullName' id='signup-fullName'
							class="login__input" required onfocus="hiddenFullNameErr()"> 
					<input type="number" placeholder='Số điện thoại' name='phoneNumber' id='signup-phoneNumber'
							class="login__input"> 
					<input type="text"
							placeholder='Địa chỉ' name='address' id='signup-address' class="login__input"> 
					<input type="date" placeholder='Ngày sinh' name='dateOfBirth' id='signup-dateOfBirth'
							class="login__input" value="2000-01-01"> 
					<input type="text"
							placeholder='Nghề nghiệp' name='career' id='signup-career' class="login__input">
						<input type="text" placeholder='Nơi làm việc' name='workingPlace' id='workingPlace'
							class="login__input">
					</div>

					<div class="login__control">
						<button type="submit" id="signup-submit" class="btn btn-md btn--warning btn--wider">Đăng
							ký</button>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


<!-- JavaScript-->
<!-- jQuery 1.9.1-->
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script>
	window.jQuery
			|| document
					.write('<script src="js/external/jquery-1.10.1.min.js"><\/script>')
</script>
<%
	if (false) {
%>
<script type="text/javascript">
	$(window).load(function() {
		$('#myModal').modal('show');
	});
</script>

<%
	}
%>
<!-- Migrate -->
<script src="js/external/jquery-migrate-1.2.1.min.js"></script>
<!-- jQuery UI -->
<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<!-- Bootstrap 3-->
<script
	src="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>

<!-- Mobile menu -->
<script src="js/jquery.mobile.menu.js"></script>
<!-- Select -->
<script src="js/external/jquery.selectbox-0.2.min.js"></script>

<script src="js/custom.js"></script>


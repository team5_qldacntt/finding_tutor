<%@page import="model.BEAN.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	User user = (User) session.getAttribute("user");
	User user1 = (User) request.getAttribute("userInfo");
	String message = (String) request.getAttribute("message");
	String message1 = (String) request.getAttribute("message1");
%>
<jsp:include page="_header.jsp"></jsp:include>


<!-- Main content -->
<section class="main-body" id="main-content" style="margin-top: 40em;">
	<div class="container">

		<!--aside infor-->
		<aside class="col-md-3">
			<div class="profile">
				<img src="images/user.png">
				<p><%=user.getFullName()%></p>
				<ul>
					<li>Nghề nghiệp: <%=user.getCareer()%></li>
					<li>Ngày sinh: <%=user.getDateOfBirth()%></li>
					<li>Kinh nghiệm: 4 năm</li>
					<li>Chuyên môn: Toán</li>
				</ul>
			</div>
			<jsp:include page="_sidebar.jsp"></jsp:include>
		</aside>
		<!--end aside-->

		<!--Main content-->
		<article class="col-md-8">
			<div class="main-content">


				<!--content-->
				<form class="form-infor" action="taikhoancanhan" method="post">
					<div class="post-item-personal col-md-12">
						<h2 style="color: #337ab7;">Thông tin người dùng</h2>
						<%
							if (message != null) {
						%>
						<div class="row">
							<b style="color: red; margin-left: 25px; font-size: medium;"><%=message%></b>
						</div>
						<%
							}
						%>
						<div class="col-md-4 text-label">
							<p>Họ tên:</p>
							<p>Ngày sinh:</p>
							<p>Địa chỉ:</p>
							<p>Số điện thoại:</p>
							<p>Nghề nghiệp:</p>
							<p>Nơi công tác:</p>
						</div>
						<div class="col-md-8 text-field">
							<input type="hidden" name="act" value="updateInfo"
								required="required"> <input type="text" name="fullName"
								value="<%=user1.getFullName()%>" required="required"> <input
								type="date" name="dateOfBirth"
								value="<%=user1.getDateOfBirth()%>" required="required" readonly>
							<input type="text" name="address" value="<%=user1.getAddress()%>"
								required="required" readonly> <input type="text"
								name="phoneNumber" value="0<%=user1.getPhoneNumber()%>"
								required="required" readonly> <input type="text"
								name="career" value="<%=user1.getCareer()%>" readonly> <input
								type="text" name="workingPlace"
								value="<%=user1.getWorkingPlace()%>" readonly>
						</div>
						<div class="btn-group col-sm-offset-10">
							<button type="button" id="button-cancel" onclick="goBack()">Quay lại</button>
						</div>
					</div>
				</form>
			</div>
		</article>
	</div>
</section>
<script type="text/javascript">
	function goBack() {
		window.history.back();
	}
</script>

<jsp:include page="_footer.jsp"></jsp:include>
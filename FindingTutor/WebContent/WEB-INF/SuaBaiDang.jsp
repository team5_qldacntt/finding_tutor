<%@page import="model.BEAN.Post"%>
<%@page import="model.BEAN.User"%>
<%@page import="model.BEAN.TimeCanTeach"%>
<%@page import="model.BEAN.PostType"%>
<%@page import="model.BEAN.PostArea"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	response.setContentType("text/html");
	response.setCharacterEncoding("UTF-8");
	request.setCharacterEncoding("UTF-8");
	ArrayList<PostArea> listPostArea = (ArrayList) request.getAttribute("listPostArea");
	ArrayList<PostType> listPostType = (ArrayList) request.getAttribute("listPostType");
	ArrayList<TimeCanTeach> listTimeCanTeach = (ArrayList) request.getAttribute("listTimeCanTeach");
	User user = (User) session.getAttribute("user");
	Post post = (Post) request.getAttribute("post");
	String message = (String) request.getAttribute("message");
%>
<jsp:include page="_header.jsp"></jsp:include>
<!-- Main content -->
<section class="main-body" id="main-content" style="margin-top: 40em;">
	<div class="container">
		<div class="col-md-6 col-md-offset-6">
			<h1>
				<strong>Đăng bài</strong>
			</h1>
		</div>
		<!--aside infor-->
		<aside class="col-md-3">
			<div class="profile">
				<img src="images/user.png">
				<p><%=user.getFullName()%></p>
				<ul>
					<li>Nghề nghiệp: <%=user.getCareer()%></li>
					<li>Công tác: <%=user.getWorkingPlace()%></li>
					<li>Ngày sinh: <%=user.getDateOfBirth()%></li>
					<li>Kinh nghiệm: 4 năm</li>
					<li>Chuyên môn: Toán</li>
				</ul>
			</div>
			<jsp:include page="_sidebar.jsp"></jsp:include>
		</aside>
		<!--end aside-->

		<!--Main content-->
		<article class="col-md-8">
			<div class="main-content">

				<!--content-->
				<form class="form-infor" action="baidangcanhan" method="post">
					<input type="hidden" name="type" value="editpost"> <input
						type="hidden" name="postID" value="<%=post.getPostID()%>">
					<div class="post-item-personal col-md-12">
						<%
							if (message != null) {
						%>
						<div class="dang-bai-line col-md-12">
							<b style="color: red; text-align: center;"><%=message%></b>
						</div>
						<%
							}
						%>
						<div class="dang-bai-line col-md-12">
							<div class="col-md-3 text-label">
								<p>Tên bài đăng:</p>
							</div>
							<div class="col-md-9 text-field">
								<input type="text" name="postTitle"
									value="<%=post.getPostTitle()%>">
							</div>
						</div>
						<div class="dang-bai-line col-md-12">
							<div class="col-md-3 text-label">
								<p>Loại bài đăng:</p>
							</div>
							<div class="col-md-9 text-field">
								<select name="postType">
									<%
										if (listPostType != null) {
											for (int i = 0; i < listPostType.size(); i++) {
									%>
									<option value="<%=listPostType.get(i).getPostTypeID()%>"
										<%=listPostType.get(i).getPostTypeID() == post.getPostTypeID() ? "checked" : ""%>><%=listPostType.get(i).getPostTypeDetail()%></option>
									<%
										}
									%>
									<%
										}
									%>
								</select>
							</div>
						</div>
						<div class="dang-bai-line col-md-12">
							<div class="col-md-3 text-label">
								<p>Khu vực (dạy/học):</p>
							</div>
							<div class="col-md-9 checkbox-group">
								<%
									if (listPostArea != null) {
										for (int i = 0; i < listPostArea.size(); i++) {
								%>
								<input type="checkbox" name="postArea"
									value="<%=listPostArea.get(i).getPostAreaID()%>"
									<%=listPostArea.get(i).getPostAreaID() == post.getPostAreaID() ? "checked" : ""%> /><%=listPostArea.get(i).getPostAreaDetail()%>
								<%
									}
								%>
								<%
									}
								%>


							</div>
						</div>
						<div class="dang-bai-line col-md-12">
							<div class="col-md-3 text-label">
								<p>Thời gian dạy/học:</p>
							</div>
							<div class="col-md-9 checkbox-group">
								<%
									if (listTimeCanTeach != null) {
										for (int i = 0; i < listTimeCanTeach.size(); i++) {
								%>
								<input name="timeCanTeach" type="checkbox"
									value="<%=listTimeCanTeach.get(i).getTimeCanTeachID()%>"
									<%=listTimeCanTeach.get(i).getTimeCanTeachID() == post.getTimeCanTeachID() ? "checked" : ""%> /><%=listTimeCanTeach.get(i).getTimeCanTeachDetail()%>
								<%
									}
								%>
								<%
									}
								%>

							</div>
						</div>
						<div class="dang-bai-line col-md-12">
							<div class="col-md-3 text-label">
								<p>Môn dạy/học:</p>
							</div>
							<div class="col-md-9 text-field">
								<input type="text" name="subjects" required="required"
									value="<%=post.getSubjects()%>">
							</div>
						</div>
						<div class="dang-bai-line col-md-12">
							<div class="col-md-3 text-label">
								<p>Nội dung bài đăng:</p>
							</div>
							<div class="col-md-9 text-field">
								<textarea style="color: black;" name="postDetail" cols="9"><%=post.getPostDetail()%></textarea>
							</div>
						</div>
						<div class="btn-group col-sm-offset-8">
							<button type="submit">Lưu thay đổi</button>
							<button type="button" onclick="goBack();"
								style="background-color: white; color: black;">Quay lại</button>
						</div>
					</div>
				</form>


			</div>
		</article>


	</div>
</section>

<jsp:include page="_footer.jsp"></jsp:include>
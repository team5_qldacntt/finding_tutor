<%@page import="model.BEAN.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	User user = (User) session.getAttribute("user");
	String message = (String) request.getAttribute("message");
	String message1 = (String) request.getAttribute("message1");
%>
<jsp:include page="_header.jsp"></jsp:include>


<!-- Main content -->
<section class="main-body" id="main-content" style="margin-top: 40em;">
	<div class="container">

		<!--aside infor-->
		<aside class="col-md-3">
			<div class="profile">
				<img src="images/user.png">
				<p><%=user.getFullName()%></p>
				<ul>
					<li>Nghề nghiệp: <%=user.getCareer()%></li>
					<li>Ngày sinh: <%=user.getDateOfBirth()%></li>
					<li>Kinh nghiệm: 4 năm</li>
					<li>Chuyên môn: Toán</li>
				</ul>
			</div>
			<jsp:include page="_sidebar.jsp"></jsp:include>
		</aside>
		<!--end aside-->

		<!--Main content-->
		<article class="col-md-8">
			<div class="main-content">


				<!--content-->
				<form class="form-infor" action="taikhoancanhan" method="post">
					<div class="post-item-personal col-md-12">
						<h2 style="color: #337ab7;">Thông tin cá nhân</h2>
						<%
							if (message != null) {
						%>
						<div class="row">
							<div class="col-sm-offset-2 col-sm-8 alert alert-success"><%=message%></div>
						</div>
						<%
							}
						%>
						<div class="col-md-4 text-label">
							<p>Họ tên*:</p>
							<p>Ngày sinh*:</p>
							<p>Địa chỉ*:</p>
							<p>Số điện thoại*:</p>
							<p>Nghề nghiệp:</p>
							<p>Nơi công tác:</p>
						</div>
						<div class="col-md-8 text-field">
							<input type="hidden" name="act" value="updateInfo" required="required"> <input
								type="text" name="fullName" value="<%=user.getFullName()%>" required="required">
							<input type="date" name="dateOfBirth"
								value="<%=user.getDateOfBirth()%>" required="required"> <input type="text"
								name="address" value="<%=user.getAddress()%>" required="required"> <input
								type="text" name="phoneNumber"
								value="0<%=user.getPhoneNumber()%>" required="required"> <input type="text"
								name="career" value="<%=user.getCareer()%>"> <input
								type="text" name="workingPlace"
								value="<%=user.getWorkingPlace()%>">
						</div>
						<div class="btn-group col-sm-offset-8">
							<button type="submit">Cập nhật</button>
							<button type="reset" id="button-cancel">Xóa hết</button>
						</div>
					</div>
				</form>
				<form class="form-infor" action="taikhoancanhan" method="post">
					<div class="post-item-personal col-md-12">
						<h2 style="color: #337ab7;">Thay đổi mật khẩu</h2>
						<%
							if (message1 != null) {
						%>
						<div class="row">
							<div class="alert alert-success"><%=message1%></div>
						</div>
						<%
							}
						%>
						<div class="col-md-4 text-label">
							<p>Mật khẩu cũ*:</p>
							<p>Mật khẩu mới*:</p>
							<p>Nhập lại mật khẩu*:</p>
						</div>
						<div class="col-md-8 text-field">
							<input type="hidden" name="act" value="changePassword"> <input
								type="password" name="oldPassword" required="required"> <input
								type="password" name="newPassword" required="required"> <input
								type="password" name="confirmPassword" required="required">
						</div>
						<div class="btn-group col-sm-offset-8">
							<button type="submit">Cập nhật</button>
							<button type="reset" id="button-cancel">Xóa hết</button>
						</div>
					</div>
				</form>
			</div>
		</article>
	</div>
</section>

<jsp:include page="_footer.jsp"></jsp:include>
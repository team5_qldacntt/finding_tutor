<%@page import="model.BEAN.User"%>
<%@page import="model.BEAN.TimeCanTeach"%>
<%@page import="model.BEAN.PostType"%>
<%@page import="model.BEAN.PostArea"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	ArrayList<PostArea> listPostArea = (ArrayList) request.getAttribute("listPostArea");
	ArrayList<PostType> listPostType = (ArrayList) request.getAttribute("listPostType");
	ArrayList<TimeCanTeach> listTimeCanTeach = (ArrayList) request.getAttribute("listTimeCanTeach");
	User user = (User) session.getAttribute("user");
	boolean isPost = (boolean) request.getAttribute("isPost");
	String message = (String) request.getAttribute("message");
%>
<jsp:include page="_header.jsp"></jsp:include>
<script>
	function Change(select) {
		/* var selectedOption = select.options[select.selectedIndex]; */
		document.getElementById("dangBaiBtn").disabled = false;
	}
	function showInputCode() {
		var code = document.getElementById("code");
		code.removeAttribute("type");
		code.setAttribute("type", "text");
	}
	function hiddenInputCode() {
		var code = document.getElementById("code");
		code.removeAttribute("type");
		code.setAttribute("type", "hidden");
	}
	
</script>

<!-- Main content -->
<section class="main-body" id="main-content" style="margin-top: 40em;">
	<div class="container">
		<div class="col-md-6 col-md-offset-6">
			<h1>
				<strong>Đăng bài</strong>
			</h1>
		</div>
		<!--aside infor-->
		<aside class="col-md-3">
			<div class="profile">
				<img src="images/user.png">
				<p><%=user.getFullName()%></p>
				<ul>
					<li>Nghề nghiệp: <%=user.getCareer()%></li>
					<li>Công tác: <%=user.getWorkingPlace()%></li>
					<li>Ngày sinh: <%=user.getDateOfBirth()%></li>
					<li>Kinh nghiệm: 4 năm</li>
					<li>Chuyên môn: Toán</li>
				</ul>
			</div>
			<jsp:include page="_sidebar.jsp"></jsp:include>
		</aside>
		<!--end aside-->

		<!--Main content-->
		<article class="col-md-8">
			<div class="main-content">

				<!--content-->
				<%
					if (message != null) {
				%>
				<p><%=message%></p>
				<%
					}
				%>
				<form class="form-infor" action="dangbai" method="get">
				<input type="hidden" name="thaoTac" value="themBaiDang">
					<div class="post-item-personal col-md-12">
						<div class="dang-bai-line col-md-12">
							<div class="col-md-3 text-label">
								<p>Tên bài đăng*:</p>
							</div>
							<div class="col-md-9 text-field">
								<input type="text" name="postTitle">
							</div>
						</div>
						<div class="dang-bai-line col-md-12">
							<div class="col-md-3 text-label">
								<p>Loại bài đăng*:</p>
							</div>
							<div class="col-md-9 text-field">
								<select name="postType">
									<%
										if (listPostType != null) {
											for (int i = 0; i < listPostType.size(); i++) {
									%>
									<option value="<%=listPostType.get(i).getPostTypeID()%>"><%=listPostType.get(i).getPostTypeDetail()%></option>
									<%
										}
									%>
									<%
										}
									%>
								</select>
							</div>
						</div>
						<div class="dang-bai-line col-md-12">
							<div class="col-md-3 text-label">
								<p>Khu vực (dạy/học)*:</p>
							</div>
							<div class="col-md-9 checkbox-group">
								<%
									if (listPostArea != null) {
										for (int i = 0; i < listPostArea.size(); i++) {
								%>
								<input type="checkbox" name="postArea"
									value="<%=listPostArea.get(i).getPostAreaID()%>" /><%=listPostArea.get(i).getPostAreaDetail()%>
								<br>
								<%
									}
								%>
								<%
									}
								%>


							</div>
						</div>
						<div class="dang-bai-line col-md-12">
							<div class="col-md-3 text-label">
								<p>Thời gian dạy/học*:</p>
							</div>
							<div class="col-md-9 checkbox-group">
								<%
									if (listTimeCanTeach != null) {
										for (int i = 0; i < listTimeCanTeach.size(); i++) {
								%>
								<input name="timeCanTeach" type="checkbox"
									value="<%=listTimeCanTeach.get(i).getTimeCanTeachID()%>" /><%=listTimeCanTeach.get(i).getTimeCanTeachDetail()%>
								<%
									}
								%>
								<%
									}
								%>

							</div>
						</div>
						<div class="dang-bai-line col-md-12">
							<div class="col-md-3 text-label">
								<p>Môn dạy/học*:</p>
							</div>
							<div class="col-md-9 text-field">
								<input type="text" name="subjects" required="required">
							</div>
						</div>
						<div class="dang-bai-line col-md-12">
							<div class="col-md-3 text-label">
								<p>Nội dung bài đăng*:</p>
							</div>
							<div class="col-md-9 text-field">
								<textarea style="color: black;" name="postDetail" cols="9"></textarea>
							</div>
						</div>
						<div class="dang-bai-line col-md-12">
							<div class="col-md-3 text-label">
								<p>Hình thức đăng bài:</p>
							</div>
							<div class="col-md-9" style="color: black;">
								<%if(isPost==true) { %>
									<input type="radio" name="typePost" value="free" onclick="hiddenInputCode()"> Đăng bài miễn phí<br>
								<%} else { %>
									<input type="radio" name="typePost" value="haveCode" onclick="showInputCode()"> Nhập code <input id="code" name="codes" type="hidden">
								<%} %>
								<br><input type="radio" name="typePost" value="haveAds" onclick="hiddenInputCode()"> Đăng bài có quảng cáo<br>
							</div>
							
						</div>
						<div class="btn-group col-sm-offset-8">
							<button id="dangBaiBtn" type="submit" >Đăng
								bài</button>
							<button type="reset" style="color: black; background-color: white;">Reset</button>
						</div>
					</div>
				</form>


			</div>
		</article>


	</div>
</section>

<jsp:include page="_footer.jsp"></jsp:include>
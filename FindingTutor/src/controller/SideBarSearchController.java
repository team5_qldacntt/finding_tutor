package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.BEAN.Post;
import model.BO.ListPostBO;

/**
 * Servlet implementation class SideBarSearchController
 */
@WebServlet("/sidebarsearch")
public class SideBarSearchController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SideBarSearchController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8");
		
		ListPostBO listPostBO = new ListPostBO();
		String act = request.getParameter("act");
		if (act == null){
			act = "";
		}
		String value = "";
		value = request.getParameter("value");
		ArrayList<Post> resultList = null;
		switch (act){
			case "district":
				resultList = listPostBO.getListPostByAreaID(value);
				request.setAttribute("listpost", resultList);
				request.getRequestDispatcher("homepage.jsp").include(request, response);
				break;
			case "subject":
				resultList = listPostBO.getListPostBySubject(value);
				request.setAttribute("listpost", resultList);
				request.getRequestDispatcher("homepage.jsp").include(request, response);
			default:
				break;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.BEAN.User;
import model.DAO.UserDAO;

/**
 * Servlet implementation class ForgotPasswordServlet
 */
@WebServlet("/ForgotPassword")
public class ForgotPasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ForgotPasswordServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8");
		request.getRequestDispatcher("/WEB-INF/QuenMatKhau.jsp").include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8");
		String email=request.getParameter("email");
		UserDAO userDAO=new UserDAO();
		User user=userDAO.getUser(email);
		if(user!=null){
			String success="Mật khẩu của bạn sẽ được gửi qua email<br> Vui lòng xem email để lấy mật khẩu!";
			request.setAttribute("success", success);
			RequestDispatcher rd=request.getRequestDispatcher("/WEB-INF/QuenMatKhau.jsp");
			rd.forward(request, response);
		}else {
			String emailNotFound="Tài khoản không tồn tại!";
			request.setAttribute("emailNotFound", emailNotFound);
			RequestDispatcher rd=request.getRequestDispatcher("/WEB-INF/QuenMatKhau.jsp");
			rd.forward(request, response);
		}
	}

}

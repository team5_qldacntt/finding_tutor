package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.BEAN.User;
import model.BO.UserBO;

/**
 * Servlet implementation class QuanLyTaiKhoanServlet
 */
@WebServlet("/quanlitaikhoan")
public class QuanLiTaiKhoanController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public QuanLiTaiKhoanController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8");
		UserBO userBO = new UserBO();
		ArrayList<User> listAllUsers;
		String act = request.getParameter("act");
		if (act == null) {
			act = "";
		}
		switch (act) {
		case "ThayDoiThongTin":
			String email = request.getParameter("email");
			String roleID = request.getParameter("roleID") == null ? "1" : "0";
			String isBanned = request.getParameter("isBanned") == null ? "0" : "1";
			String isDeleted = request.getParameter("isDeleted") == null ? "0" : "1";
			System.out.println("Email: " + email + "\tRoleID: " + roleID + "\tIsBanned: " + isBanned + "\tIsDeleted: "
					+ isDeleted);
			String message;
			String userName = userBO.viewUserInformation(email).getFullName();
			if (userBO.updateUserInformation(email, roleID, isBanned, isDeleted)){
				message="Cập nhật thông tin người dùng " + userName + " thành công!";
			} else {
				message="Cập nhật thông tin người dùng có tên" + userName + " thất bại!";
			}
			request.setAttribute("message", message);
			listAllUsers = userBO.getAllUsers();
			request.setAttribute("listAllUsers", listAllUsers);
			request.getRequestDispatcher("/WEB-INF/QuanLiTaiKhoan.jsp").include(request, response);
			break;
		case "timkiem":
			String searchContent = request.getParameter("searchContent").trim();
			String typeOfUserSelection = request.getParameter("typeOfUserSelection");
			String isBannedSelection = request.getParameter("isBannedSelection");
			String isDeletedSelection = request.getParameter("isDeletedSelection");
			System.out.println(searchContent + "\t" + typeOfUserSelection + "\t" + isBannedSelection + "\t" + isDeletedSelection);
			request.setAttribute("searchContent", searchContent);
			request.setAttribute("typeOfUserSelection", typeOfUserSelection);
			request.setAttribute("isBannedSelection", isBannedSelection);
			request.setAttribute("isDeletedSelection", isDeletedSelection);
			searchContent = "".equals(searchContent) ? "%" : "%" + searchContent + "%";
			typeOfUserSelection = "-1".equals(typeOfUserSelection) ? "%" : "%" + typeOfUserSelection + "%";
			isBannedSelection = "-1".equals(isBannedSelection) ? "%" : "%" + isBannedSelection + "%";
			isDeletedSelection = "-1".equals(isDeletedSelection) ? "%" : "%" + isDeletedSelection + "%";
			listAllUsers = userBO.searchUser(searchContent, typeOfUserSelection, isBannedSelection, isDeletedSelection);
//			response.getWriter().print(listAllUsers);
			request.setAttribute("listAllUsers", listAllUsers);
			request.getRequestDispatcher("/WEB-INF/QuanLiTaiKhoan.jsp").include(request, response);
			break;
		default:
			listAllUsers = userBO.getAllUsers();
			request.setAttribute("listAllUsers", listAllUsers);
			request.getRequestDispatcher("/WEB-INF/QuanLiTaiKhoan.jsp").include(request, response);
			break;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

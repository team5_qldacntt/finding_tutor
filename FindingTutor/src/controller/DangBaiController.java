package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.BEAN.Post;
import model.BEAN.PostArea;
import model.BEAN.PostType;
import model.BEAN.TimeCanTeach;
import model.BEAN.User;
import model.BO.DangBaiBO;
import model.BO.UserBO;
import utilities.Utilities;

/**
 * Servlet implementation class dangbai
 */
@WebServlet("/dangbai")
public class DangBaiController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DangBaiBO dangBaiBO;
	UserBO userBO;
	String email;
	ArrayList<PostArea> listPostArea;
	ArrayList<PostType> listPostType;
	ArrayList<TimeCanTeach> listTimeCanTeach;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DangBaiController() {
		super();
		dangBaiBO = new DangBaiBO();
		userBO = new UserBO();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		String thaoTac = request.getParameter("thaoTac");
		boolean isPost = dangBaiBO.checkIsPost(user.getEmail());
		if (user.getEmail() == null) {
			response.sendRedirect("homepage.jsp");
		} else if (thaoTac == null) {
			request.setAttribute("user", userBO.viewUserInformation(user.getEmail()));
			listPostArea = dangBaiBO.getListPostAreaBO();
			listPostType = dangBaiBO.getListPostTypeBO();
			listTimeCanTeach = dangBaiBO.getListTimeCanTeach();
			request.setAttribute("isPost", isPost);
			request.setAttribute("listPostArea", listPostArea);
			request.setAttribute("listPostType", listPostType);
			request.setAttribute("listTimeCanTeach", listTimeCanTeach);
			request.getRequestDispatcher("/WEB-INF/dangbai.jsp").include(request, response);
		} else {
			String postTitle = request.getParameter("postTitle");
			String postTypeID = request.getParameter("postType");
			String postAreaID = request.getParameter("postArea");
			String timeCanTeachID = request.getParameter("timeCanTeach");
			String subjects = request.getParameter("subjects");
			String postDetail = request.getParameter("postDetail");
			String email = user.getEmail();
			Post post = new Post();
			post.setDate(utilities.Utilities.getCurrentDate());
			post.setEmail(email);
			post.setPostAreaID(Integer.parseInt(postAreaID));
			post.setPostDetail(postDetail);
			post.setPostTypeID(Integer.parseInt(postTypeID));
			post.setPostTitle(postTitle);
			post.setSubjects(subjects);
			post.setDate(utilities.Utilities.getCurrentDate());
			post.setTimeCanTeachID(Integer.parseInt(timeCanTeachID));
			String typePost = request.getParameter("typePost");
			if (typePost.equalsIgnoreCase("free")) {
				if (dangBaiBO.addPost(post) != 0) {
					request.getRequestDispatcher("/baidangcanhan").include(request, response);
				} else {
					request.setAttribute("message", "Lỗi kết nối cơ sở dữ liệu !");
					request.getRequestDispatcher("/dangbai").include(request, response);
				}
			} else if (typePost.equalsIgnoreCase("haveCode")) {
				String code = request.getParameter("codes");
				if (code.equalsIgnoreCase("abc123")) {
					post.setCode(code);
					if (dangBaiBO.addPost(post) != 0) {
						request.getRequestDispatcher("/baidangcanhan").include(request, response);
					} else {
						request.setAttribute("message", "Lỗi kết nối cơ sở dữ liệu !");
						request.getRequestDispatcher("/dangbai").include(request, response);
					}
				} else {
					listPostArea = dangBaiBO.getListPostAreaBO();
					listPostType = dangBaiBO.getListPostTypeBO();
					listTimeCanTeach = dangBaiBO.getListTimeCanTeach();
					request.setAttribute("isPost", isPost);
					request.setAttribute("message", "code không đúng, vui lòng kiểm tra lại !");
					request.setAttribute("listPostArea", listPostArea);
					request.setAttribute("listPostType", listPostType);
					request.setAttribute("listTimeCanTeach", listTimeCanTeach);
					request.getRequestDispatcher("/WEB-INF/dangbai.jsp").include(request, response);
				}
			} else if (typePost.equalsIgnoreCase("haveAds")) {
				request.setAttribute("currentPost", post);
				request.getRequestDispatcher("/BuyAdsController").include(request, response);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.BEAN.AdvertisementType;
import model.BEAN.User;
import model.BEAN.UserAds;
import model.BO.BuyAdsBO;

/**
 * Servlet implementation class updateAds
 */
@WebServlet("/updateAds")
public class UpdateAdsController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateAdsController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		String result = request.getParameter("result");
		BuyAdsBO buyAdsBO = new BuyAdsBO();
		if("thanhCong".equalsIgnoreCase(result)) {
			UserAds userAds = new UserAds();
			String email = request.getParameter("email");
			String adsID = request.getParameter("AdsID");
			userAds.setEmail(email);
			AdvertisementType advertisementType = new AdvertisementType();
			advertisementType.setAdvertisementTypeID(Integer.parseInt(adsID));
			userAds.setAdvertisementType(advertisementType);
			if(buyAdsBO.addUserAds(userAds) != 0) {
				ArrayList<UserAds> listUserAds = buyAdsBO.getAllAdsByUser(email);
				request.setAttribute("listUserAds", listUserAds);
				request.getRequestDispatcher("/WEB-INF/muaquangcao.jsp").include(request, response);
			} 
		} else {
			ArrayList<UserAds> listUserAds = buyAdsBO.getAllAdsByUser(user.getEmail());
			String message = "Mua quảng cáo thất bại: ";
			request.setAttribute("message", message);
			request.setAttribute("listUserAds", listUserAds);
			request.getRequestDispatcher("/WEB-INF/muaquangcao.jsp").include(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.BEAN.Post;
import model.BO.ListPostBO;

/**
 * Servlet implementation class ShowHomePageServlet
 */
@WebServlet("/homepage")
public class ShowHomePageController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ShowHomePageController() {
		super();
		// TODO Auto-generated constructor stub  
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		try {
			ListPostBO listPost = new ListPostBO();
			ArrayList<Post> list;
			int postType;
			String idFildter = request.getParameter("idFildter");
			String searchKey = request.getParameter("searchKey");
			// System.out.println(idFildter);
			String page = request.getParameter("page");

			if (searchKey == null || "null".equals(searchKey)) {
				if (idFildter == null || "0".equals(idFildter)) {
					list = listPost.getListPostBO();
				} else {
					postType = Integer.parseInt(idFildter);
					list = listPost.getListPostByPostTypeId(postType);
					request.setAttribute("idFildter", idFildter);
				}

			} else {
				request.setAttribute("searchKey", searchKey);
				list = listPost.searchListPost(searchKey);
			}
			if (page != null) {
				request.setAttribute("page", page);
				if (page == "1") {
					for (int i = 0; i < 5 && i < list.size(); i++) {
						if (list.get(i).getadvertisementNumber() > -1)
							listPost.increaseAdvertisementNumber(list.get(i).getPostID(),
									list.get(i).getadvertisementNumber() + 1);
					}
				}
			}else{
				for (int i = 0; i < 5 && i < list.size(); i++) {
					if (list.get(i).getadvertisementNumber() > -1)
						listPost.increaseAdvertisementNumber(list.get(i).getPostID(),
								list.get(i).getadvertisementNumber() + 1);
				}
			}

			request.setAttribute("listpost", list);
			RequestDispatcher rd = request.getRequestDispatcher("homepage.jsp");
			rd.forward(request, response);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}

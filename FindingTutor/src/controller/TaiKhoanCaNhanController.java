package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.BEAN.User;
import model.BO.UserBO;
import utilities.Utilities;

/**
 * Servlet implementation class TaiKhoanCaNhanServlet
 */
@WebServlet("/taikhoancanhan")
public class TaiKhoanCaNhanController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TaiKhoanCaNhanController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8");
		UserBO userBO = new UserBO();
		HttpSession session = request.getSession();
		String message = "";
		String email = (String) session.getAttribute("username");
		User user;
		String act = request.getParameter("act");
		if (act == null) {
			act = "";
		}
		switch (act) {
		case "updateInfo":
			String fullName = request.getParameter("fullName");
			String dateOfBirth = request.getParameter("dateOfBirth");
			if (dateOfBirth.compareTo(Utilities.getCurrentDate()) > 0 || dateOfBirth.compareTo("1900-01-01") < 0) {
				message = "Ngày sinh nhập vào không hợp lệ!";
			} else {
				String address = request.getParameter("address");
				String phoneNumber = request.getParameter("phoneNumber");
				try {
					int phone = Integer.parseInt(phoneNumber);
					String career = request.getParameter("career");
					String workingPlace = request.getParameter("workingPlace");
					user = (User) session.getAttribute("user");
					email = user.getEmail();
					if (userBO.updateUserInformation(email, fullName, phone, address, dateOfBirth, career,
							workingPlace)) {
						message = "Cập nhật thông tin thành công!";
					} else {
						message = "Cập nhật thông tin thất bại!";
					}
					user = userBO.viewUserInformation(email);
					session.setAttribute("user", user);
				} catch (NumberFormatException e) {
					message = "Số điện thoại nhập vào bị sai!";
				}
			}
			request.setAttribute("message", message);
			request.getRequestDispatcher("/WEB-INF/TaiKhoanCaNhan.jsp").include(request, response);
			break;
		case "changePassword":
			String oldPassword = request.getParameter("oldPassword");
			String newPassword = request.getParameter("newPassword");
			String confirmPassword = request.getParameter("confirmPassword");
			if (newPassword.equals(confirmPassword)) {
				if (userBO.changePasswordBO(email, oldPassword, newPassword)) {
					message = "Cập nhật mật khẩu thành công!";
				} else {
					message = "Mật khẩu nhập vào bị sai!";
				}
			} else {
				message = "Mật khẩu nhập lại không trùng khớp!";
			}
			request.setAttribute("message1", message);
			user = userBO.viewUserInformation(email);
			request.setAttribute("user", user);
			request.getRequestDispatcher("/WEB-INF/TaiKhoanCaNhan.jsp").include(request, response);
			break;
		case "userInfo":
			email = request.getParameter("email");
			request.setAttribute("userInfo", userBO.getUserByEmail(email));
			request.getRequestDispatcher("/WEB-INF/XemThongTinNguoiDung.jsp").include(request, response);
			break;
		default:
			user = userBO.viewUserInformation(email);
			request.setAttribute("user", user);
			request.getRequestDispatcher("/WEB-INF/TaiKhoanCaNhan.jsp").include(request, response);
			break;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

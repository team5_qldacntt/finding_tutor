package controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.BEAN.User;
import model.BO.AddUserBO;
import utilities.Utilities;

/**
 * Servlet implementation class AddNewUser
 */
@WebServlet("/Register")
public class AddNewUserController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddNewUserController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8");
		String email = request.getParameter("email");
		// int roleID=Integer.parseInt(request.getParameter("role"));
		String password = request.getParameter("password");
		String fullName = request.getParameter("fullName");
		int phoneNumber;
		try {
			phoneNumber = Integer.parseInt(request.getParameter("phoneNumber"));
		} catch (Exception e) {
			// TODO: handle exception
			phoneNumber = 0;
			if (request.getParameter("phoneNumber") == "")
				;
		}
		String address = request.getParameter("address");
		// String avatarFileName = request.getParameter("avatarFileName");
		String dateOfBirth = request.getParameter("dateOfBirth");
		String career = request.getParameter("career");
		String workingPlace = request.getParameter("workingPlace");
		User user = new User(email, 1, password, fullName, dateOfBirth, career, workingPlace, phoneNumber, address, "",
				false, false);
		AddUserBO add = new AddUserBO();
		Utilities utilities = new Utilities();

			if (add.addUser(user)) {
				request.setAttribute("success", "  ");
				RequestDispatcher rd = request.getRequestDispatcher("homepage");
				rd.forward(request, response);
			} else {
				request.setAttribute("message", "Tài khoản đã tồn tại");
				RequestDispatcher rd = request.getRequestDispatcher("homepage");
				rd.forward(request, response);
			}

	}

}

package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.BEAN.Post;
import model.BO.CheckLoginBO;
import model.BO.ListPostBO;
import model.BO.UserBO;

/**
 * Servlet implementation class CheckLoginServlet
 */
@WebServlet("/CheckLogin")
public class CheckLoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CheckLoginController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String email = request.getParameter("user-email");
		String password = request.getParameter("user-password");
		CheckLoginBO checkLoginBO = new CheckLoginBO();
		UserBO userBO = new UserBO();
		try {
			ListPostBO listPost = new ListPostBO();
			ArrayList<Post> list = listPost.getListPostBO();
			request.setAttribute("listpost", list);
			if (checkLoginBO.checkLoginBO(email, password)) {
				HttpSession session = request.getSession();
				// session.setAttribute("username", email);
				session.setAttribute("user", userBO.viewUserInformation(email));
				RequestDispatcher rd = request.getRequestDispatcher("homepage.jsp");
				rd.forward(request, response);
			} else {
				response.getWriter().write("0");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}

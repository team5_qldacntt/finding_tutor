package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.BEAN.Post;
import model.BEAN.User;
import model.BO.BuyAdsBO;

/**
 * Servlet implementation class BuyAdsController
 */
@WebServlet("/BuyAdsController")
public class BuyAdsController extends HttpServlet {
	BuyAdsBO buyAdsBO;
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyAdsController() {
        super();
        buyAdsBO = new BuyAdsBO();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		String buyAdsResult = request.getParameter("buyAdsResult");
		if(buyAdsResult == null) {
			request.getRequestDispatcher("/WEB-INF/nganhang.jsp").include(request, response);
		} else if(buyAdsResult.equalsIgnoreCase("thanhCong")){
			String postTitle = request.getParameter("postTitle");
			String postTypeID = request.getParameter("postTypeID");
			String postAreaID = request.getParameter("postAreaID");
			String timeCanTeachID = request.getParameter("timeCanTeachID");
			String subjects = request.getParameter("subjects");
			String postDetail = request.getParameter("postDetail");
			String email = user.getEmail();
			Post post = new Post();
			post.setDate(utilities.Utilities.getCurrentDate());
			post.setEmail(email);
			post.setPostAreaID(Integer.parseInt(postAreaID));
			post.setPostDetail(postDetail);
			post.setPostTypeID(Integer.parseInt(postTypeID));
			post.setPostTitle(postTitle);
			post.setSubjects(subjects);
			post.setDate(utilities.Utilities.getCurrentDate());
			post.setTimeCanTeachID(Integer.parseInt(timeCanTeachID));
			post.setadvertisementNumber(0);
			if(buyAdsBO.addPost(post) != 0) {
				request.getRequestDispatcher("/baidangcanhan").include(request, response);
			} else {
				request.setAttribute("message", "lỗi kết nối cơ sở dữ liệu ");
				request.getRequestDispatcher("/dangbai").include(request, response);
			}
		} else if(buyAdsResult.equalsIgnoreCase("thatBai")) {
			request.setAttribute("message", "mua quảng cáo thất bại ! vui lòng kiểm tra lại  ");
			request.getRequestDispatcher("/dangbai").include(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

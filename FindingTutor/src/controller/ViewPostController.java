package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.BEAN.Post;
import model.BEAN.PostArea;
import model.BEAN.PostType;
import model.BEAN.TimeCanTeach;
import model.BO.DangBaiBO;
import model.BO.ViewPostBO;

/**
 * Servlet implementation class ViewPostServlet
 */
@WebServlet("/ViewPost")
public class ViewPostController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ViewPostController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8");
		
		ArrayList<PostArea> listPostArea;
		ArrayList<PostType> listPostType;
		ArrayList<TimeCanTeach> listTimeCanTeach;
		
		DangBaiBO dangBaiBO = new DangBaiBO();

		ViewPostBO viewPostBO = new ViewPostBO();
		String act = request.getParameter("act");
		String postID = request.getParameter("viewPostId");
		System.out.println("PostID to view: " + postID);
		try {
			Post post = viewPostBO.getPost(Integer.parseInt(postID));
			request.setAttribute("post", post);
		} catch (NumberFormatException e) {
			System.out.println(e.getMessage());
		}
		String message = "";
		if (act == null){
			act = "";
		}
		switch (act){
			case "edit":
				listPostArea = dangBaiBO.getListPostAreaBO();
				listPostType = dangBaiBO.getListPostTypeBO();
				listTimeCanTeach = dangBaiBO.getListTimeCanTeach();
				request.setAttribute("listPostArea", listPostArea);
				request.setAttribute("listPostType", listPostType);
				request.setAttribute("listTimeCanTeach", listTimeCanTeach);
				request.getRequestDispatcher("/WEB-INF/SuaBaiDang.jsp").include(request, response);
				break;
			case "deletepost":
				postID = request.getParameter("id");
				System.out.println("PostID to delete: " + postID);
				PrintWriter printWriter = response.getWriter();
				if (viewPostBO.deletePostByPostID(postID)){
					printWriter.print("Xóa bài đăng thành công!");
				} else {
					printWriter.print("Xóa bài đăng thất bại!");
				}
				break;
			case "closepost":
				postID = request.getParameter("id");
				if (viewPostBO.closePostByPostID(postID)){
					message = "Bài đăng đã được đóng và không còn hiển thị/cho phép comment!";
				} else {
					message = "Lỗi!";
				}
				request.setAttribute("message", message);
				request.getRequestDispatcher("baidangcanhan").include(request, response);
				break;
			case "openpost":
				postID = request.getParameter("id");
				if (viewPostBO.openPostByPostID(postID)){
					message = "Bài đăng đã được mở lại và sẽ được hiển thị trên trang chủ!";
				} else {
					message = "Lỗi!";
				}
				request.setAttribute("message", message);
				request.getRequestDispatcher("baidangcanhan").include(request, response);
				break;
			case "reportpost":
				break;
			default:
				request.getRequestDispatcher("/WEB-INF/baiDangChiTiet.jsp").include(request, response);
				break;
		}
	}

}

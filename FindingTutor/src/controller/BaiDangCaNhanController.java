package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.BEAN.Post;
import model.BEAN.User;
import model.BO.ListPostBO;
import model.BO.ReportBO;
import model.BO.ViewPostBO;

/**
 * Servlet implementation class BaiDangCaNhan
 */
@WebServlet("/baidangcanhan")
public class BaiDangCaNhanController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BaiDangCaNhanController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		ArrayList<Post> listPost;
		User user = (User) session.getAttribute("user");
		String type = request.getParameter("type");
		if (type == null){
			type = "";
		}
		ListPostBO listPostBO = new ListPostBO();
		ReportBO reportBO = new ReportBO();
		ViewPostBO viewPostBO = new ViewPostBO();
		switch (type){
			case "giasu":
				listPost = listPostBO.getListTutorPostByUserID(user.getEmail());
				request.setAttribute("listPost", listPost);
				request.setAttribute("typeOfListPost", "Bài đăng tìm kiếm suất dạy");
				request.getRequestDispatcher("/WEB-INF/DanhSachBaiDangCaNhan.jsp").include(request, response);
				break;
			case "suatday":
				listPost = listPostBO.getListNotTutorPostByUserID(user.getEmail());
				request.setAttribute("listPost", listPost);
				request.setAttribute("typeOfListPost", "Bài đăng tìm kiếm gia sư");
				request.getRequestDispatcher("/WEB-INF/DanhSachBaiDangCaNhan.jsp").include(request, response);
				break;
			case "report":
				listPost = reportBO.getListPostIsReported(user.getEmail());
				request.setAttribute("listPost", listPost);
				request.setAttribute("typeOfListPost", "Bài đăng bị cảnh cáo");
				request.getRequestDispatcher("/WEB-INF/DanhSachBaiDangCaNhan.jsp").include(request, response);
				break;
			case "editpost":
				String postID = request.getParameter("postID");
				String postTitle = request.getParameter("postTitle");
				String postTypeID = request.getParameter("postType");
				String postAreaID = request.getParameter("postArea");
				String timeCanTeachID = request.getParameter("timeCanTeach");
				String subjects = request.getParameter("subjects");
				String postDetail = request.getParameter("postDetail");
				String message = "";
				if (viewPostBO.updatePostByPostID(postID, postTitle, postTypeID, postAreaID, timeCanTeachID, subjects, postDetail)){
					message = "Cập nhật bài đăng thành công!";
				} else {
					message = "Cập nhật bài đăng thất bại!";
				}
				request.setAttribute("message", message);
				System.out.println(postAreaID + "\n" + postTypeID + "\n" + postTitle + "\n" + timeCanTeachID + "\n" + subjects + "\n" + postDetail);
				listPost = listPostBO.getListTutorPostByUserID(user.getEmail());
				request.setAttribute("listPost", listPost);
				request.getRequestDispatcher("/WEB-INF/DanhSachBaiDangCaNhan.jsp").include(request, response);
				break;
			default:
				listPost = listPostBO.getListPostByUserID(user.getEmail());
				request.setAttribute("listPost", listPost);
				request.getRequestDispatcher("/WEB-INF/DanhSachBaiDangCaNhan.jsp").include(request, response);
				break;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

/**
 * 
 */
package model.DAO;

/**
 * @author Quang Ngo TP
 *
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Database {
	Connection connection = null;
	Statement statement = null;
	
	public Database(){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/findingtutor?useUnicode=true&characterEncoding=utf-8&SSL=false", "root", "");
			statement = connection.createStatement();
		} catch (Exception e) { 
			System.err.println("Error while connecting database: " + e.getMessage());
		}
	}
	
	public int update(String query) throws SQLException{
		return statement.executeUpdate(query);
	}
	
	public ResultSet execute(String query) throws SQLException{
		ResultSet resultSet = statement.executeQuery(query);
		return resultSet;
	}
	
	public void close() throws SQLException{
		if (statement != null) {
			statement.close();
		}
		if (connection != null) {
			connection.close();
		}
	}
}


package model.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import model.BEAN.Post;
import utilities.Utilities;

public class PostDAO extends Database {

	public Post getPost(int postID) {

		Post post;
		try {
			ResultSet resultSet = execute("SELECT * FROM post WHERE postid=" + postID);
			if (resultSet.next()) {
				String email = resultSet.getString(2);
				String postTitle = resultSet.getString(3);
				String subjects = resultSet.getString(11);
				int postTypeID = resultSet.getInt(4);
				int postAreaID = resultSet.getInt(5);
				String postDetail = resultSet.getString(7);
				// String postCode = resultSet.getString("postcode");
				int advertisementTypeID = resultSet.getInt(10);
				int timeCanTeachID = resultSet.getInt(12);
				boolean isClosed = resultSet.getBoolean(9);
				String date = resultSet.getString(6);
				post = new Post(postID, email, postTitle, subjects, postTypeID, postAreaID, postDetail, date,
						advertisementTypeID, timeCanTeachID);
				post.setClosed(isClosed);
				return post;
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	// public ArrayList<Post> getListPost() throws SQLException {
	// ArrayList<Post> result = new ArrayList<Post>();
	// Post post;
	// ResultSet resultSet = execute("SELECT * FROM post WHERE IsClosed=0 ORDER
	// BY Date DESC");
	// while (resultSet.next()) {
	// int postID = resultSet.getInt(1);
	// String email = resultSet.getString(2);
	// String postTitle = resultSet.getString(3);
	// String subjects = resultSet.getString(11);
	// int postTypeID = resultSet.getInt(4);
	// int postAreaID = resultSet.getInt(5);
	// String postDetail = resultSet.getString(7);
	// // String postCode = resultSet.getString("postcode");
	// int advertisementTypeID = resultSet.getInt(10);
	// int timeCanTeachID = resultSet.getInt(12);
	// boolean isClosed = resultSet.getBoolean(9);
	// String date = resultSet.getString(6);
	// post = new Post(postID, email, postTitle, subjects, postTypeID,
	// postAreaID, postDetail, date,
	// advertisementTypeID, timeCanTeachID);
	// post.setClosed(isClosed);
	// result.add(post);
	// }
	// return result;
	// }

	public ArrayList<Post> getListPost() {
		ArrayList<Post> result = new ArrayList<Post>();
		try {
			ResultSet resultSet = execute(
					"SELECT * FROM post WHERE AdvertisementNumber > -1 AND IsClosed = 0 ORDER BY AdvertisementNumber ASC");
			listAdPost(result, resultSet, 5);
			resultSet = execute("SELECT * FROM post WHERE subjects LIKE '%ly' AND IsClosed = 0");
			hotPost(result, resultSet, 5);
			resultSet = execute("SELECT * FROM post WHERE IsClosed = 0 ORDER BY date DESC");
			resultSet.last();
			int size=resultSet.getRow();
			resultSet.beforeFirst();
			listAdPost(result, resultSet, size);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return null;
		}
		return result;
	}

	public void listAdPost(ArrayList<Post> result, ResultSet resultSet, int numPost) {
		Post post;
		int size = result.size();
		int i, j;
		// int j = i + 5;
		try {
			for (i = 0; i < numPost && resultSet.next(); i++) {
				int postID = resultSet.getInt(1);
				String email1 = resultSet.getString(2);
				String postTitle = resultSet.getString(3);
				String subjects = resultSet.getString(11);
				int postTypeID = resultSet.getInt(4);
				int postAreaID = resultSet.getInt(5);
				String postDetail = resultSet.getString(7);
				// String postCode = resultSet.getString("postcode");
				int advertisementTypeID = resultSet.getInt(10);
				int timeCanTeachID = resultSet.getInt(12);
				boolean isClosed = resultSet.getBoolean(9);
				String date = resultSet.getString(6);
				post = new Post(postID, email1, postTitle, subjects, postTypeID, postAreaID, postDetail, date,
						advertisementTypeID, timeCanTeachID);
				post.setClosed(isClosed);
				
				for (j = 0; j < size; j++) {
					if (result.get(j).getPostID() == post.getPostID()) {
						i--;
						break;
					}
				}
				if (j == size) {
					result.add(post);
				}

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void hotPost(ArrayList<Post> result, ResultSet resultSet, int numPost) {
		Post post;
		int i=0, j=0;
		int size=result.size();
		int[] randOfNumPost = new int[numPost];
		try {
			resultSet.last();
			int count = resultSet.getRow();
			resultSet.beforeFirst();
			Random random = new Random();
			if (count < numPost)
				for (i = 0; i < numPost && resultSet.next(); i++) {
					int postID = resultSet.getInt(1);
					String email1 = resultSet.getString(2);
					String postTitle = resultSet.getString(3);
					String subjects = resultSet.getString(11);
					int postTypeID = resultSet.getInt(4);
					int postAreaID = resultSet.getInt(5);
					String postDetail = resultSet.getString(7);
					// String postCode =
					// resultSet.getString("postcode");
					int advertisementTypeID = resultSet.getInt(10);
					int timeCanTeachID = resultSet.getInt(12);
					boolean isClosed = resultSet.getBoolean(9);
					String date = resultSet.getString(6);
					post = new Post(postID, email1, postTitle, subjects, postTypeID, postAreaID, postDetail, date,
							advertisementTypeID, timeCanTeachID);
					post.setClosed(isClosed);
					for (j = 0; j < size; j++) {
						if (result.get(j).getPostID() == post.getPostID()) {
							i--;
							break;
						}
					}
					if (j == size) {
						result.add(post);
					}

				}
			else {
				for (int k = 0; k < 5; k++) {
					randOfNumPost[k] = random.nextInt(count);
				}

				Arrays.sort(randOfNumPost);
				for (i = 0; i < numPost && resultSet.next(); i++) {
					if (i + 1 == randOfNumPost[j++]) {
						int postID = resultSet.getInt(1);
						String email1 = resultSet.getString(2);
						String postTitle = resultSet.getString(3);
						String subjects = resultSet.getString(11);
						int postTypeID = resultSet.getInt(4);
						int postAreaID = resultSet.getInt(5);
						String postDetail = resultSet.getString(7);
						// String postCode =
						// resultSet.getString("postcode");
						int advertisementTypeID = resultSet.getInt(10);
						int timeCanTeachID = resultSet.getInt(12);
						boolean isClosed = resultSet.getBoolean(9);
						String date = resultSet.getString(6);
						post = new Post(postID, email1, postTitle, subjects, postTypeID, postAreaID, postDetail, date,
								advertisementTypeID, timeCanTeachID);
						post.setClosed(isClosed);
						for (j = 0; j < size; j++) {
							if (result.get(j).getPostID() == post.getPostID()) {
								i--;
								break;
							}
						}
						if (j == size) {
							result.add(post);
						}
					}
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @param email
	 * @return
	 */
	public ArrayList<Post> getListPostByUserID(String email) {
		ArrayList<Post> result = new ArrayList<Post>();
		Post post;
		try {
			ResultSet resultSet = execute("SELECT * FROM post WHERE Email='" + email + "' ORDER BY Date DESC");
			while (resultSet.next()) {
				int postID = resultSet.getInt(1);
				String email1 = resultSet.getString(2);
				String postTitle = resultSet.getString(3);
				String subjects = resultSet.getString(11);
				int postTypeID = resultSet.getInt(4);
				int postAreaID = resultSet.getInt(5);
				String postDetail = resultSet.getString(7);
				// String postCode = resultSet.getString("postcode");
				int advertisementTypeID = resultSet.getInt(10);
				int timeCanTeachID = resultSet.getInt(12);
				boolean isClosed = resultSet.getBoolean(9);
				String date = resultSet.getString("Date");
				post = new Post(postID, email1, postTitle, subjects, postTypeID, postAreaID, postDetail, date,
						advertisementTypeID, timeCanTeachID);
				post.setClosed(isClosed);
				result.add(post);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return null;
		}
		return result;
	}

	public ArrayList<Post> getListTutorPostByUserID(String email) {
		ArrayList<Post> result = new ArrayList<Post>();
		Post post;
		try {
			ResultSet resultSet = execute(
					"SELECT * FROM post WHERE Email='" + email + "' AND PostTypeID=1 ORDER BY Date DESC");
			while (resultSet.next()) {
				int postID = resultSet.getInt(1);
				String email1 = resultSet.getString(2);
				String postTitle = resultSet.getString(3);
				String subjects = resultSet.getString(11);
				int postTypeID = resultSet.getInt(4);
				int postAreaID = resultSet.getInt(5);
				String postDetail = resultSet.getString(7);
				// String postCode = resultSet.getString("postcode");
				int advertisementTypeID = resultSet.getInt(10);
				int timeCanTeachID = resultSet.getInt(12);
				boolean isClosed = resultSet.getBoolean(9);
				String date = resultSet.getString(6);
				post = new Post(postID, email1, postTitle, subjects, postTypeID, postAreaID, postDetail, date,
						advertisementTypeID, timeCanTeachID);
				post.setClosed(isClosed);
				result.add(post);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return null;
		}
		return result;
	}

	public ArrayList<Post> getListNotTutorPostByUserID(String email) {
		ArrayList<Post> result = new ArrayList<Post>();
		Post post;
		try {
			ResultSet resultSet = execute(
					"SELECT * FROM post WHERE Email='" + email + "' AND PostTypeID=2 ORDER BY Date DESC");
			while (resultSet.next()) {
				int postID = resultSet.getInt(1);
				String email1 = resultSet.getString(2);
				String postTitle = resultSet.getString(3);
				String subjects = resultSet.getString(11);
				int postTypeID = resultSet.getInt(4);
				int postAreaID = resultSet.getInt(5);
				String postDetail = resultSet.getString(7);
				// String postCode = resultSet.getString("postcode");
				int advertisementTypeID = resultSet.getInt(10);
				int timeCanTeachID = resultSet.getInt(12);
				boolean isClosed = resultSet.getBoolean(9);
				String date = resultSet.getString("Date");
				post = new Post(postID, email1, postTitle, subjects, postTypeID, postAreaID, postDetail, date,
						advertisementTypeID, timeCanTeachID);
				post.setClosed(isClosed);
				result.add(post);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return null;
		}
		return result;
	}

	/**
	 * @param postType
	 * @return
	 */
	public ArrayList<Post> getListPostByPostTypeId(int postType) {
		ArrayList<Post> result = new ArrayList<Post>();
		Post post;
		try {
			ResultSet resultSet = execute(
					"SELECT * FROM post WHERE PostTypeID='" + postType + "' AND IsClosed=0 ORDER BY Date DESC");
			while (resultSet.next()) {
				int postID = resultSet.getInt(1);
				String email1 = resultSet.getString(2);
				String postTitle = resultSet.getString(3);
				String subjects = resultSet.getString(11);
				int postTypeID = resultSet.getInt(4);
				int postAreaID = resultSet.getInt(5);
				String postDetail = resultSet.getString(7);
				// String postCode = resultSet.getString("postcode");
				int advertisementTypeID = resultSet.getInt(10);
				int timeCanTeachID = resultSet.getInt(12);
				boolean isClosed = resultSet.getBoolean(9);
				String date = resultSet.getString("Date");
				post = new Post(postID, email1, postTitle, subjects, postTypeID, postAreaID, postDetail, date,
						advertisementTypeID, timeCanTeachID);
				post.setClosed(isClosed);
				result.add(post);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return null;
		}
		return result;
	}

	/**
	 * @param postID
	 * @param postTitle
	 * @param postTypeID
	 * @param postAreaID
	 * @param timeCanTeachID
	 * @param subjects
	 * @param postDetail
	 * @return
	 */
	public boolean updatePostByPostID(String postID, String postTitle, String postTypeID, String postAreaID,
			String timeCanTeachID, String subjects, String postDetail) {
		String query = "UPDATE post SET PostTitle='" + postTitle + "', PostTypeID='" + postTypeID + "', PostAreaID='"
				+ postAreaID + "', TimeCanTeachID='" + timeCanTeachID + "', Subjects='" + subjects + "', PostDetail='"
				+ postDetail + "' WHERE PostID='" + postID + "' AND IsClosed=0";
		try {
			return update(query) > 0 ? true : false;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

	/**
	 * @param email
	 * @return
	 */
	public boolean checkLastDatePost(String email) {
		boolean result = false;
		String query = "SELECT DATE FROM POST WHERE EMAIL='" + email + "' AND DATE='"
				+ utilities.Utilities.getCurrentDate() + "'";
		try {
			ResultSet resultSet = execute(query);
			if (resultSet.next()) {
				result = false;
			} else {
				result = true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	public ArrayList<Post> searchListPost(String searchKey) {
		ArrayList<Post> result = new ArrayList<Post>();
		Post post;

		try {
			ResultSet resultSet = execute("SELECT * FROM post WHERE email like'%" + searchKey
					+ "%' OR posttypeid in (SELECT posttypeid FROM posttype where posttypedetail like '%" + searchKey
					+ "%') OR postareaid in (SELECT postareaid FROM postarea where postareadetail like '%" + searchKey
					+ "%') OR date like '%" + searchKey + "%' OR postdetail like '%" + searchKey
					+ "%' OR posttitle like '%" + searchKey + "%' AND IsClosed=0");
			while (resultSet.next()) {
				int postID = resultSet.getInt(1);
				String email1 = resultSet.getString(2);
				String postTitle = resultSet.getString(3);
				String subjects = resultSet.getString(11);
				int postTypeID = resultSet.getInt(4);
				int postAreaID = resultSet.getInt(5);
				String postDetail = resultSet.getString(7);
				// String postCode = resultSet.getString("postcode");
				int advertisementTypeID = resultSet.getInt(10);
				int timeCanTeachID = resultSet.getInt(12);
				boolean isClosed = resultSet.getBoolean(9);
				String date = resultSet.getString("Date");
				post = new Post(postID, email1, postTitle, subjects, postTypeID, postAreaID, postDetail, date,
						advertisementTypeID, timeCanTeachID);
				post.setClosed(isClosed);
				result.add(post);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return null;
		}
		return result;
	}

	public int addPostToDatabase(Post post) {
		int ressult = 0;
		String query = "INSERT INTO POST(EMAIL, POSTTITLE, POSTTYPEID, POSTAREAID, DATE, POSTDETAIL, CODES, ADVERTISEMENTNUMBER, SUBJECTS, TIMECANTEACHID) VALUE('"
				+ post.getEmail() + "', '" + post.getPostTitle() + "', " + post.getPostTypeID() + ", "
				+ post.getPostAreaID() + ", '" + post.getDate() + "', '" + post.getPostDetail() + "', '"
				+ post.getCode() + "', " + post.getadvertisementNumber() + ", '" + post.getSubjects() + "', "
				+ post.getTimeCanTeachID() + ")";
		try {
			ressult = update(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ressult;
	}

	/**
	 * @param postID
	 * @return
	 */
	public boolean deletePostByPostID(String postID) {
		String query = "DELETE FROM post WHERE PostID='" + postID + "'";
		try {
			return update(query) > 0 ? true : false;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

	public boolean closePostByPostID(String postID) {
		String query = "UPDATE post SET IsClosed = 1 WHERE PostID='" + postID + "'";
		try {
			return update(query) > 0 ? true : false;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

	public boolean openPostByPostID(String postID) {
		String query = "UPDATE post SET IsClosed = 0 WHERE PostID='" + postID + "'";
		try {
			return update(query) > 0 ? true : false;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

	/**
	 * @param value
	 * @return
	 */
	public ArrayList<Post> getListPostByAreaID(String value) {
		Post post = null;
		ArrayList<Post> listResult = null;
		String query = "SELECT * FROM post WHERE PostAreaID='" + value + "'";
		try {
			ResultSet resultSet = execute(query);
			listResult = new ArrayList<>();
			while (resultSet.next()) {
				int postID = resultSet.getInt(1);
				String email1 = resultSet.getString(2);
				String postTitle = resultSet.getString(3);
				String subjects = resultSet.getString(11);
				int postTypeID = resultSet.getInt(4);
				int postAreaID = resultSet.getInt(5);
				String postDetail = resultSet.getString(7);
				// String postCode = resultSet.getString("postcode");
				int advertisementTypeID = resultSet.getInt(10);
				int timeCanTeachID = resultSet.getInt(12);
				boolean isClosed = resultSet.getBoolean(9);
				String date = resultSet.getString("Date");
				post = new Post(postID, email1, postTitle, subjects, postTypeID, postAreaID, postDetail, date,
						advertisementTypeID, timeCanTeachID);
				post.setClosed(isClosed);
				listResult.add(post);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listResult;
	}

	/**
	 * @param value
	 * @return
	 */
	public ArrayList<Post> getListPostBySubject(String value) {
		String query = "SELECT * FROM post WHERE Subjects LIKE '%" + value + "%'";
		Post post = null;
		ArrayList<Post> listResult = null;
		try {
			ResultSet resultSet = execute(query);
			listResult = new ArrayList<>();
			while (resultSet.next()) {
				int postID = resultSet.getInt(1);
				String email1 = resultSet.getString(2);
				String postTitle = resultSet.getString(3);
				String subjects = resultSet.getString(11);
				int postTypeID = resultSet.getInt(4);
				int postAreaID = resultSet.getInt(5);
				String postDetail = resultSet.getString(7);
				// String postCode = resultSet.getString("postcode");
				int advertisementTypeID = resultSet.getInt(10);
				int timeCanTeachID = resultSet.getInt(12);
				boolean isClosed = resultSet.getBoolean(9);
				String date = resultSet.getString("Date");
				post = new Post(postID, email1, postTitle, subjects, postTypeID, postAreaID, postDetail, date,
						advertisementTypeID, timeCanTeachID);
				post.setClosed(isClosed);
				listResult.add(post);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listResult;
	}
	public boolean increaseAdvertisementNumber(int postId, int advertisementNumber){
		String query = "UPDATE post SET AdvertisementNumber = "+advertisementNumber+" WHERE PostID='" + postId + "'";
		try {
			return update(query) > 0 ? true : false;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
}

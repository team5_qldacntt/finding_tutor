/**
 * 
 */
package model.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;

import model.BEAN.AdvertisementType;

/**
 * @author chinh_huynh
 *
 */
public class AdvertisementTypeDAO extends Database{

	/**
	 * @param int1
	 * @return
	 */
	public AdvertisementType getadvertisementTypebyID(int int1) {
		AdvertisementType advertisementType = new AdvertisementType();
		String query = "select * from advertisementtype where AdvertisementTypeID='"+int1+"';";
		try {
			ResultSet resultSet = execute(query);
			if (resultSet.next()){
				advertisementType.setAdvertisementTypeID(resultSet.getInt(1));
				advertisementType.setAdvertisementTypeDetail(resultSet.getString(2));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return advertisementType;
	}

}

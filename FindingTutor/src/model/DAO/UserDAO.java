/**
 * 
 */
package model.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.BEAN.User;
import utilities.Utilities;

/**
 * @author Quang Ngo TP
 *
 */
public class UserDAO extends Database {
	public boolean checkLogin(User user) {
		boolean result = false;
		String query = "SELECT * FROM USER WHERE EMAIL='" + user.getEmail() + "' AND PASSWORDS = '" + user.getPassword()
				+ "'";
		try {
			ResultSet resultSet = execute(query);
			if (resultSet.next()) {
				result = true;
			} else {
				result = false;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	public User getUser(String email) {
		String sql = "SELECT * FROM user WHERE Email='" + email + "'";
		try {
			ResultSet rs = execute(sql);
			if (rs.next()) {
				int roleID = rs.getInt(2);
				String password = rs.getString(3);
				String fullName = rs.getString(4);
				String dateOfBirth = rs.getString(5);
				String career = rs.getString(6);
				String workingPlace = rs.getString(7);
				int phoneNumber = rs.getInt(8);
				String address = rs.getString(9);
				String avatarFileName = rs.getString(10);
				boolean isBanned = rs.getInt(11) == 1 ? true : false;
				boolean isDeleted = rs.getInt(12) == 1 ? true : false;
				return new User(email, roleID, password, fullName, dateOfBirth, career, workingPlace, phoneNumber,
						address, avatarFileName, isBanned, isDeleted);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	public boolean updateUserInformation(String email, String fullName, String dateOfBirth, String career,
			String workingPlace, int phoneNumber, String address, String avatarFileName) {
		String sql = "UPDATE user SET FullName='" + fullName + "', PhoneNumber='" + phoneNumber + "', Address='"
				+ address + "', DateOfBirth='" + dateOfBirth + "', Career='" + career + "', WorkingPlace='"
				+ workingPlace + "' WHERE Email='" + email + "'";
		try {
			return update(sql) > 0 ? true : false;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

	public boolean changePassword(String email, String password) {
		String sql = "UPDATE user SET Password='" + password + "' WHERE Email='" + email + "'";
		try {
			return update(sql) > 0 ? true : false;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

	public boolean isValidAccount(String email, String password) throws Exception {
		boolean result;

		ResultSet rs = execute("SELECT * FROM User WHERE email= '" + email + "' AND password='" + password + "'");
		if (rs.next())
			result = true;
		else
			result = false;
		return result;
	}

	/**
	 * @return
	 */
	public ArrayList<User> getListAllUsers() {

		String query = "SELECT * FROM user ORDER BY RoleID";
		try {
			ResultSet rs = execute(query);
			ArrayList<User> listAllUsers = new ArrayList<>();
			while (rs.next()) {
				String email = rs.getString(1);
				int roleID = rs.getInt(2);
				String password = rs.getString(3);
				String fullName = rs.getString(4);
				String dateOfBirth = rs.getString(5);
				String career = rs.getString(6);
				String workingPlace = rs.getString(7);
				int phoneNumber = rs.getInt(8);
				String address = rs.getString(9);
				String avatarFileName = rs.getString(10);
				boolean isBanned = rs.getInt(11) == 1 ? true : false;
				boolean isDeleted = rs.getInt(12) == 1 ? true : false;
				listAllUsers.add(new User(email, roleID, password, fullName, dateOfBirth, career, workingPlace,
						phoneNumber, address, avatarFileName, isBanned, isDeleted));
			}
			return listAllUsers;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	/**
	 * @param email
	 * @param roleID
	 * @param isBanned
	 * @param isDeleted
	 * @return
	 */
	public boolean updateUserInformation(String email, String roleID, String isBanned, String isDeleted) {
		String query = "UPDATE user SET RoleID='" + roleID + "', isBanned='" + isBanned + "', isDeleted='" + isDeleted
				+ "' WHERE Email='" + email + "'";
		try {
			return update(query) > 0 ? true : false;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

	/**
	 * @param searchContent
	 * @param typeOfUserSelection
	 * @param isBannedSelection
	 * @param isDeletedSelection
	 * @return
	 */
	public ArrayList<User> searchUser(String searchContent, String typeOfUserSelection, String isBannedSelection,
			String isDeletedSelection) {
		String query1 = "SELECT * FROM user WHERE Email LIKE '" + searchContent + "' AND RoleID LIKE '"
				+ typeOfUserSelection + "' AND IsBanned LIKE '" + isBannedSelection + "' AND IsDeleted LIKE '"
				+ isDeletedSelection + "' ORDER BY RoleID ASC";
		String query2 = "SELECT * FROM user WHERE FullName LIKE '" + searchContent + "' AND RoleID LIKE '"
				+ typeOfUserSelection + "' AND IsBanned LIKE '" + isBannedSelection + "' AND IsDeleted LIKE '"
				+ isDeletedSelection + "' ORDER BY RoleID ASC";
		try {
			ArrayList<User> listUsers = new ArrayList<>();
			ResultSet rs = execute(query1);
			while (rs.next()) {
				String email = rs.getString(1);
				int roleID = rs.getInt(2);
				String password = rs.getString(3);
				String fullName = rs.getString(4);
				String dateOfBirth = rs.getString(5);
				String career = rs.getString(6);
				String workingPlace = rs.getString(7);
				int phoneNumber = rs.getInt(8);
				String address = rs.getString(9);
				String avatarFileName = rs.getString(10);
				boolean isBanned = rs.getInt(11) == 1 ? true : false;
				boolean isDeleted = rs.getInt(12) == 1 ? true : false;
				listUsers.add(new User(email, roleID, password, fullName, dateOfBirth, career, workingPlace,
						phoneNumber, address, avatarFileName, isBanned, isDeleted));
			}
			rs = execute(query2);
			while (rs.next()) {
				String email = rs.getString(1);
				int roleID = rs.getInt(2);
				String password = rs.getString(3);
				String fullName = rs.getString(4);
				String dateOfBirth = rs.getString(5);
				String career = rs.getString(6);
				String workingPlace = rs.getString(7);
				int phoneNumber = rs.getInt(8);
				String address = rs.getString(9);
				String avatarFileName = rs.getString(10);
				boolean isBanned = rs.getInt(11) == 1 ? true : false;
				boolean isDeleted = rs.getInt(12) == 1 ? true : false;
				if (!Utilities.checkListUser(listUsers, email)) {
					listUsers.add(new User(email, roleID, password, fullName, dateOfBirth, career, workingPlace,
							phoneNumber, address, avatarFileName, isBanned, isDeleted));
				}
			}
			return listUsers;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	public boolean addUser(User user) {
		try {
			int isBanned = 0;
			int isDeleted = 0;
			if (user.isBanned())
				isBanned = 1;
			if (user.isDeleted())
				isDeleted = 1;
			update("INSERT INTO user VALUES ( '" + user.getEmail() + "' , '" + user.getRoleID() + "' , '"
					+ user.getPassword() + "' , '" + user.getFullName() + "' , '" + user.getDateOfBirth() + "' , '"
					+ user.getCareer() + "' , '" + user.getWorkingPlace() + "' , '" + user.getPhoneNumber() + "' , '"
					+ user.getAddress() + "' , '" + user.getavatarFileName() + "' , '" + isBanned + "' , '" + isDeleted
					+ "');");
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println("Loi addUser,UserDAO");
			return false;
		}
	}
}

/**
 * 
 */
package model.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.BEAN.PostArea;
import model.BEAN.PostType;
import model.BEAN.TimeCanTeach;

/**
 * @author chinh_huynh
 *
 */
public class DangbaiDAO extends Database{

	/**
	 * @return
	 */
	public ArrayList<PostType> getAllPostType() {
		ArrayList<PostType> listPostType = new ArrayList<>();
		String query = "SELECT * FROM POSTTYPE";
		try {
			ResultSet resultSet = execute(query);
			while(resultSet.next()) {
				PostType postType = new PostType();
				postType.setPostTypeID(resultSet.getInt(1));
				postType.setPostTypeDetail(resultSet.getNString(2));
				listPostType.add(postType);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listPostType;
	}

	/**
	 * @return
	 */
	public ArrayList<PostArea> getAllPostArea() {
		ArrayList<PostArea> listPostArea = new ArrayList<>();
		String query = "SELECT * FROM POSTAREA";
		try {
			ResultSet resultSet = execute(query);
			while(resultSet.next()) {
				PostArea postArea = new PostArea();
				postArea.setPostAreaID(resultSet.getInt(1));
				postArea.setPostAreaDetail(resultSet.getNString(2));
				listPostArea.add(postArea);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listPostArea;
	}

	/**
	 * @return
	 */
	public ArrayList<TimeCanTeach> getAllTimeCanTeach() {
		ArrayList<TimeCanTeach> listTimeCanTeach = new ArrayList<>();
		String query = "SELECT * FROM TIMECANTEACH";
		try {
			ResultSet resultSet = execute(query);
			while(resultSet.next()) {
				TimeCanTeach timeCanTeach = new TimeCanTeach();
				timeCanTeach.setTimeCanTeachID(resultSet.getInt(1));
				timeCanTeach.setTimeCanTeachDetail(resultSet.getNString(2));
				listTimeCanTeach.add(timeCanTeach);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listTimeCanTeach;
	}

}

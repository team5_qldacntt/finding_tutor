/**
 * 
 */
package model.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.BEAN.PostArea;

/**
 * @author chinh_huynh
 *
 */
public class PostAreaDAO extends Database{

	/**
	 * @return
	 */
	public ArrayList<PostArea> getAllPostArea() {
		ArrayList<PostArea> listPostArea = new ArrayList<>();
		String query = "SELECT * FROM postarea";
		try {
			ResultSet resultSet = execute(query);
			while(resultSet.next()) {
				PostArea postArea = new PostArea();
				postArea.setPostAreaID(resultSet.getInt(1));
				postArea.setPostAreaDetail(resultSet.getString(2));
				listPostArea.add(postArea);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listPostArea;
	}
	
	public String getPostAreaById(int Id) throws SQLException{
		ResultSet resultSet=execute("SELECT PostAreaDetail FROM postarea WHERE PostAreaID="+Id);
		if(resultSet.next())return resultSet.getString("PostAreaDetail");
		return null;
	}

}

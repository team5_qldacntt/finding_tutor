/**
 * 
 */
package model.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.BEAN.PostType;

/**
 * @author chinh_huynh
 *
 */
public class PostTypeDAO extends Database{

	/**
	 * @return
	 */
	public ArrayList<PostType> getAllPostType() {
		ArrayList<PostType> listPostType = new ArrayList<>();
		String query = "SELECT * FROM posttype";
		try {
			ResultSet resultSet = execute(query);
			while(resultSet.next()) {
				PostType postType = new PostType();
				postType.setPostTypeID(resultSet.getInt(1));
				postType.setPostTypeDetail(resultSet.getString(2));
				listPostType.add(postType);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listPostType;
	}
	
	public String getPostTypeByID(int ID) throws SQLException{
		ResultSet rs= execute("SELECT PostTypeDetail FROM posttype WHERE PostTypeID="+ID);
		if(rs.next())return rs.getString("PostTypeDetail");
		return null;
	}

}

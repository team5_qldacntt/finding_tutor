/**
 * 
 */
package model.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.BEAN.TimeCanTeach;

/**
 * @author chinh_huynh
 *
 */
public class TimeCanTeachDAO extends Database {

	/**
	 * @return
	 */
	public ArrayList<TimeCanTeach> getAllTimeCanTeach() {
		ArrayList<TimeCanTeach> listTime = new ArrayList<>();
		String query = "SELECT * FROM timecanteach";
		try {
			ResultSet resultSet = execute(query);
			while (resultSet.next()) {
				TimeCanTeach timeCanTeach = new TimeCanTeach();
				timeCanTeach.setTimeCanTeachID(resultSet.getInt(1));
				timeCanTeach.setTimeCanTeachDetail(resultSet.getString(2));
				listTime.add(timeCanTeach);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listTime;
	}

	public String getTimeCanTeachById(int Id) throws SQLException {
		ResultSet reSet=execute("SELECT timecanteachdetail FROM timecanteach where timecanteachid="+Id);
		if(reSet.next())return reSet.getString("timecanteachdetail");
		return null;
	}
}

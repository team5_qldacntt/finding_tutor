/**
 * 
 */
package model.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.BEAN.UserAds;

/**
 * @author chinh_huynh
 *
 */
public class UserAdsDAO extends Database {

	private AdvertisementTypeDAO advertisementTypeDAO = new AdvertisementTypeDAO();

	/**
	 * @param email
	 * @return
	 */
	public ArrayList<UserAds> getAllAdsByUser(String email) {
		ArrayList<UserAds> list = new ArrayList<>();
		String query = "SELECT * FROM USERADS WHERE EMAIL='" + email + "'";
		try {
			ResultSet resultSet = execute(query);
			while (resultSet.next()) {
				UserAds userAds = new UserAds();
				userAds.setEmail(resultSet.getString(2));
				userAds.setUserAdsID(resultSet.getInt(1));
				userAds.setAdvertisementType(advertisementTypeDAO.getadvertisementTypebyID(resultSet.getInt(3)));
				// System.out.println(advertisementTypeDAO.getadvertisementTypebyID(resultSet.getInt(3)).getAdvertisementTypeID());
				userAds.setQuantity(resultSet.getInt(4));
				list.add(userAds);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * @param userAds
	 * @return
	 */
	public int addUserAds(UserAds userAds) {
		int result = 0;
		String query = "UPDATE USERADS SET QUANTITY=QUANTITY+5 WHERE AdvertisementTypeID="
				+ userAds.getAdvertisementType().getAdvertisementTypeID() + " AND EMAIL='" + userAds.getEmail() + "'";
		try {
			result = update(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

}

package model.BO;
import model.DAO.UserDAO;

public class CheckLoginBO {
	public boolean checkLoginBO(String email, String password) throws Exception{
		UserDAO checkLoginDAO=new UserDAO();
		return checkLoginDAO.isValidAccount(email,password);
	}
}

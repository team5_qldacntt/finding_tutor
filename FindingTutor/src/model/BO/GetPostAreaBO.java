/**
 * 
 */
package model.BO;

import java.sql.SQLException;

import model.DAO.PostAreaDAO;

/**
 * @author Quang Ngo TP
 *
 */
public class GetPostAreaBO {
	public String getPostAreaById(int Id) throws SQLException{
		PostAreaDAO pDao=new PostAreaDAO();
		String result=pDao.getPostAreaById(Id);
		pDao.close();
		return result;
	}
}

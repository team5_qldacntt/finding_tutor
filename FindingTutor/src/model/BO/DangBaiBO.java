/**
 * 
 */
package model.BO;

import java.util.ArrayList;

import model.BEAN.Post;
import model.BEAN.PostArea;
import model.BEAN.PostType;
import model.BEAN.TimeCanTeach;
import model.DAO.PostAreaDAO;
import model.DAO.PostDAO;
import model.DAO.PostTypeDAO;
import model.DAO.TimeCanTeachDAO;

/**
 * @author chinh_huynh
 *
 */
public class DangBaiBO {

	private PostAreaDAO postAreaDAO;
	private PostTypeDAO postTypeDAO;
	private TimeCanTeachDAO timeCanTeachDAO;
	private PostDAO postDAO;

	
	public DangBaiBO() {
		super();
		postAreaDAO = new PostAreaDAO();
		postTypeDAO = new PostTypeDAO();
		timeCanTeachDAO = new TimeCanTeachDAO();
		postDAO = new PostDAO();
	}


	/**
	 * @return
	 */
	public ArrayList<PostArea> getListPostAreaBO() {
		return postAreaDAO.getAllPostArea();
	}


	/**
	 * @return
	 */
	public ArrayList<PostType> getListPostTypeBO() {
		return postTypeDAO.getAllPostType();
	}


	/**
	 * @return
	 */
	public ArrayList<TimeCanTeach> getListTimeCanTeach() {
		return timeCanTeachDAO.getAllTimeCanTeach();
	}


	/**
	 * @param post
	 */
	public int addPost(Post post) {
		return postDAO.addPostToDatabase(post);
	}


	/**
	 * @param email
	 * @return
	 */
	public boolean checkIsPost(String email) {
		return postDAO.checkLastDatePost(email);
	}

}

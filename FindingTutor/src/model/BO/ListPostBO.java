package model.BO;

import java.sql.SQLException;
import java.util.ArrayList;

import model.BEAN.Post;
import model.DAO.PostDAO;

public class ListPostBO {
	PostDAO postDAO = new PostDAO();
	public ArrayList<Post> getListPostBO() throws SQLException {
		return postDAO.getListPost();
	}

	/**
	 * @param email
	 */
	public ArrayList<Post> getListPostByUserID(String email) {
		return postDAO.getListPostByUserID(email);
	}
	
	public ArrayList<Post> getListTutorPostByUserID(String email) {
		return postDAO.getListTutorPostByUserID(email);
	}
	
	public ArrayList<Post> getListNotTutorPostByUserID(String email) {
		return postDAO.getListNotTutorPostByUserID(email);
	}

	/**
	 * @param postType
	 * @return
	 */
	public ArrayList<Post> getListPostByPostTypeId(int postType) {
		return postDAO.getListPostByPostTypeId(postType);
	}
	
	public ArrayList<Post> searchListPost(String searchKey){
		return postDAO.searchListPost(searchKey);
	}

	/**
	 * @param value
	 * @return
	 */
	public ArrayList<Post> getListPostByAreaID(String value) {
		return postDAO.getListPostByAreaID(value);
	}

	/**
	 * @param value
	 * @return
	 */
	public ArrayList<Post> getListPostBySubject(String value) {
		return postDAO.getListPostBySubject(value);
	}
	public boolean increaseAdvertisementNumber(int postId, int advertisementNumber) {
		return postDAO.increaseAdvertisementNumber(postId, advertisementNumber);
	}
}

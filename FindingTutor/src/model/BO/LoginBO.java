/**
 * 
 */
package model.BO;


import model.BEAN.User;
import model.DAO.UserDAO;

/**
 * @author chinh_huynh
 *
 */
public class LoginBO {
	UserDAO userDAO;

	public LoginBO() {
		userDAO = new UserDAO();

	}

	/**
	 * @param user
	 * @return
	 */
	public String checkValidates(User user) {
		String message = "";
		if ("".equalsIgnoreCase(user.getEmail()) && "".equalsIgnoreCase(user.getPassword())) {
			message = "email and password not empty !";
		} else if ("".equalsIgnoreCase(user.getPassword())) {
			message = "password not empty !";
		} else if ("".equalsIgnoreCase(user.getEmail())) {
			message = "email not empty";
		}
		return message;
	}

	/**
	 * @param user
	 * @return
	 */
	public boolean checkLoginBO(User user) {
		return userDAO.checkLogin(user);
	}

}

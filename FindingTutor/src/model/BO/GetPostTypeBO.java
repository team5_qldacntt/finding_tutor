/**
 * 
 */
package model.BO;

import java.sql.SQLException;

import model.DAO.PostTypeDAO;

/**
 * @author Quang Ngo TP
 *
 */
public class GetPostTypeBO {
	public String getPostTypeByID(int Id) throws SQLException{
		PostTypeDAO postTypeDAO=new PostTypeDAO();
		String result=postTypeDAO.getPostTypeByID(Id);
		postTypeDAO.close();
		return result;
	}
}

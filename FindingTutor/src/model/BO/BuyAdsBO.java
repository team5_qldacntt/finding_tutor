/**
 * 
 */
package model.BO;

import java.util.ArrayList;

import model.BEAN.Post;
import model.BEAN.UserAds;
import model.DAO.PostDAO;
import model.DAO.UserAdsDAO;

/**
 * @author chinh_huynh
 *
 */
public class BuyAdsBO {

	private UserAdsDAO userAdsDAO;
	private PostDAO postDAO;
	
	
	

	public BuyAdsBO() {
		super();
		userAdsDAO = new UserAdsDAO();
		postDAO = new PostDAO();
	}



	/**
	 * @param email
	 */
	public ArrayList<UserAds> getAllAdsByUser(String email) {
		return userAdsDAO.getAllAdsByUser(email);
		
	}



	/**
	 * @param userAds
	 */
	public int addUserAds(UserAds userAds) {
		return userAdsDAO.addUserAds(userAds);
		
	}



	/**
	 * @param post
	 * @return
	 */
	public int addPost(Post post) {
		return postDAO.addPostToDatabase(post) ;
	}
	

}

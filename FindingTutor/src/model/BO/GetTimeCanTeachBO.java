/**
 * 
 */
package model.BO;

import java.sql.SQLException;

import model.DAO.TimeCanTeachDAO;

/**
 * @author Quang Ngo TP
 *
 */
public class GetTimeCanTeachBO {
	public String getTimeCanTeachById(int Id) throws SQLException {
		TimeCanTeachDAO timeCanTeachDao=new TimeCanTeachDAO();
		String result=timeCanTeachDao.getTimeCanTeachById(Id);
		timeCanTeachDao.close();
		return result;
	}
}

/**
 * 
 */
package model.BO;

import java.util.ArrayList;

import model.BEAN.Post;
import model.DAO.PostDAO;
import model.DAO.ReportDAO;

/**
 * @author Quang Ngo TP
 *
 */
public class ReportBO {
	ReportDAO reportDAO = new ReportDAO();
	PostDAO postDAO = new PostDAO();
	public ArrayList<Post> getListPostIsReported(String email){
		ArrayList<Post> listAllPost = postDAO.getListPostByUserID(email);
		ArrayList<Post> resultList = new ArrayList<>();
		for (Post post : listAllPost) {
			if (reportDAO.checkWhetherPostIsReported(post.getPostID())){
				resultList.add(post);
			}
		}
		return resultList;
	}
}

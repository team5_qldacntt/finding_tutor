/**
 * 
 */
package model.BO;

import java.util.ArrayList;

import model.BEAN.User;
import model.DAO.UserDAO;

/**
 * @author Quang Ngo TP
 *
 */
public class UserBO {
	UserDAO userDAO;

	public UserBO() {
		userDAO = new UserDAO();
	}

	public boolean updateUserInformation(String email, String fullName, int phoneNumber, String address,
			String dateOfBirth, String career, String workingPlace) {
		return userDAO.updateUserInformation(email, fullName, dateOfBirth, career, workingPlace, phoneNumber, address, "");
	}

	public User viewUserInformation(String email) {
		return userDAO.getUser(email);
	}

	public boolean changePasswordBO(String email, String oldPassword, String newPassword) {
		if (!userDAO.getUser(email).getPassword().equals(oldPassword)){
			System.out.println("Wrong password");
			return false;
		}
		return userDAO.changePassword(email, newPassword);
	}

	/**
	 * @return
	 */
	public ArrayList<User> getAllUsers() {
		return userDAO.getListAllUsers();
	}

	/**
	 * @param email
	 * @param roleID
	 * @param isBanned
	 * @param isDeleted
	 * @return
	 */
	public boolean updateUserInformation(String email, String roleID, String isBanned, String isDeleted) {
		return userDAO.updateUserInformation(email, roleID, isBanned, isDeleted);
	}

	/**
	 * @param searchContent
	 * @param typeOfUserSelection
	 * @param isBannedSelection
	 * @param isDeletedSelection
	 * @return
	 */
	public ArrayList<User> searchUser(String searchContent, String typeOfUserSelection, String isBannedSelection, String isDeletedSelection){
		return userDAO.searchUser(searchContent, typeOfUserSelection, isBannedSelection, isDeletedSelection);
	}

	/**
	 * @param email
	 * @return
	 */
	public Object getUserByEmail(String email) {
		return userDAO.getUser(email);
	}
}

package model.BO;

import model.BEAN.Post;
import model.DAO.PostDAO;

public class ViewPostBO {
	PostDAO vpDao = new PostDAO();

	public Post getPost(int postID) {
		return vpDao.getPost(postID);
	}

	public boolean updatePostByPostID(String postID, String postTitle, String postTypeID, String postAreaID,
			String timeCanTeachID, String subjects, String postDetail) {
		return vpDao.updatePostByPostID(postID, postTitle, postTypeID, postAreaID, timeCanTeachID, subjects, postDetail);
	}

	/**
	 * @param postID
	 * @return
	 */
	public boolean deletePostByPostID(String postID) {
		return vpDao.deletePostByPostID(postID);
	}
	
	/**
	 * @param postID
	 * @return
	 */
	public boolean closePostByPostID(String postID) {
		return vpDao.closePostByPostID(postID);
	}
	
	/**
	 * @param postID
	 * @return
	 */
	public boolean openPostByPostID(String postID) {
		return vpDao.openPostByPostID(postID);
	}
}

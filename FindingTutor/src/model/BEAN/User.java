package model.BEAN;

/**
 * @author chinh_huynh
 *
 */

public class User {
	private String email;
	private int roleID;
	private String password;
	private String fullName;
	private String dateOfBirth, career, workingPlace;
	private int phoneNumber;
	private String address;
	private String avatarFileName;
	private boolean isBanned;
	private boolean isDeleted;

	public User() {
		super();
	}

	public User(String email, int roleID, String password, String fullName, String dateOfBirth, String career,
			String workingPlace, int phoneNumber, String address, String avatarFileName, boolean isBanned,
			boolean isDeleted) {
		this.email = email;
		this.roleID = roleID;
		this.password = password;
		this.fullName = fullName;
		this.dateOfBirth = dateOfBirth;
		this.career = career;
		this.workingPlace = workingPlace;
		this.phoneNumber = phoneNumber;
		this.address = address;
		this.avatarFileName = avatarFileName;
		this.isBanned = isBanned;
		this.isDeleted = isDeleted;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getRoleID() {
		return roleID;
	}

	public void setRoleID(int roleID) {
		this.roleID = roleID;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public int getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getavatarFileName() {
		return avatarFileName;
	}

	public void setavatarFileName(String avatarFileName) {
		this.avatarFileName = avatarFileName;
	}

	public boolean isBanned() {
		return isBanned;
	}

	public void setBanned(boolean isBanned) {
		this.isBanned = isBanned;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getCareer() {
		return career;
	}

	public void setCareer(String career) {
		this.career = career;
	}

	public String getWorkingPlace() {
		return workingPlace;
	}

	public void setWorkingPlace(String workingPlace) {
		this.workingPlace = workingPlace;
	}

	public String getAvatarFileName() {
		return avatarFileName;
	}

	public void setAvatarFileName(String avatarFileName) {
		this.avatarFileName = avatarFileName;
	}
	
	
}

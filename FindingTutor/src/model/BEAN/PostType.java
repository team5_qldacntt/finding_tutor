/**
 * 
 */
package model.BEAN;

/**
 * @author chinh_huynh
 *
 */
public class PostType {
	private int postTypeID;
	private String postTypeDetail;

	public PostType() {
		super();
	}

	public PostType(int postTypeID, String postTypeDetail) {
		super();
		this.postTypeID = postTypeID;
		this.postTypeDetail = postTypeDetail;
	}

	public int getPostTypeID() {
		return postTypeID;
	}

	public void setPostTypeID(int postTypeID) {
		this.postTypeID = postTypeID;
	}

	public String getPostTypeDetail() {
		return postTypeDetail;
	}

	public void setPostTypeDetail(String postTypeDetail) {
		this.postTypeDetail = postTypeDetail;
	}

}

/**
 * 
 */
package model.BEAN;

/**
 * @author chinh_huynh
 *
 */
public class PostArea {
	private String postAreaDetail;
	private int postAreaID;

	public PostArea(int postAreaID, String postAreaDetail) {
		super();
		this.postAreaID = postAreaID;
		this.postAreaDetail = postAreaDetail;
	}

	public PostArea() {
		super();
	}

	public int getPostAreaID() {
		return postAreaID;
	}

	public void setPostAreaID(int postAreaID) {
		this.postAreaID = postAreaID;
	}

	public String getPostAreaDetail() {
		return postAreaDetail;
	}

	public void setPostAreaDetail(String postAreaDetail) {
		this.postAreaDetail = postAreaDetail;
	}
	
}

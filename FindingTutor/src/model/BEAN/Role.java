/**
 * 
 */
package model.BEAN;

/**
 * @author Quang Ngo TP
 *
 */
public class Role {
	private String roleDetail;
	private int roleID;

	public Role(int roleID, String roleDetail) {
		super();
		this.roleID = roleID;
		this.roleDetail = roleDetail;
	}

	public Role() {
		super();
	}

	public int getRoleID() {
		return roleID;
	}

	public void setRoleID(int roleID) {
		this.roleID = roleID;
	}

	public String getRoleDetail() {
		return roleDetail;
	}

	public void setRoleDetail(String roleDetail) {
		this.roleDetail = roleDetail;
	}
	
}

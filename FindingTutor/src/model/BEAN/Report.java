package model.BEAN;

/**
 * @author Quang Ngo TP
 *
 */

public class Report {
	private String email, reportDescription;
	private int reportID, postID;

	public Report(int reportID, int postID, String email, String reportDescription) {
		super();
		this.reportID = reportID;
		this.postID = postID;
		this.email = email;
		this.reportDescription = reportDescription;
	}

	public Report() {
		super();
	}

	public int getReportID() {
		return reportID;
	}

	public void setReportID(int reportID) {
		this.reportID = reportID;
	}

	public int getPostID() {
		return postID;
	}

	public void setPostID(int postID) {
		this.postID = postID;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getReportDescription() {
		return reportDescription;
	}

	public void setReportDescription(String reportDescription) {
		this.reportDescription = reportDescription;
	}
	
}

/**
 * 
 */
package model.BEAN;

/**
 * @author chinh_huynh
 *
 */
public class Post {
	private String email, postTitle, postDetail, code, subjects, date;
	private boolean isClosed;
	private int postID, postTypeID, postAreaID, advertisementNumber, timeCanTeachID;

	public Post() {
		super();
	}

	public Post(int postID, String email, String postTitle, String subjects, int postTypeID, int postAreaID,
			String postDetail, String date, int advertisementNumber, int timeCanTeachID) {
		super();
		this.postID = postID;
		this.email = email;
		this.postTitle = postTitle;
		this.postTypeID = postTypeID;
		this.postAreaID = postAreaID;
		this.postDetail = postDetail;
		this.date = date;
		this.advertisementNumber = advertisementNumber;
		this.isClosed = false;
		this.timeCanTeachID = timeCanTeachID;
		this.subjects = subjects;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getPostID() {
		return postID;
	}

	public void setPostID(int postID) {
		this.postID = postID;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPostTitle() {
		return postTitle;
	}

	public void setPostTitle(String postTitle) {
		this.postTitle = postTitle;
	}

	public int getPostTypeID() {
		return postTypeID;
	}

	public void setPostTypeID(int postTypeID) {
		this.postTypeID = postTypeID;
	}

	public int getPostAreaID() {
		return postAreaID;
	}

	public void setPostAreaID(int postAreaID) {
		this.postAreaID = postAreaID;
	}

	public String getPostDetail() {
		return postDetail;
	}

	public void setPostDetail(String postDetail) {
		this.postDetail = postDetail;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getadvertisementNumber() {
		return advertisementNumber;
	}

	public void setadvertisementNumber(int advertisementNumber) {
		this.advertisementNumber = advertisementNumber;
	}

	public boolean isClosed() {
		return isClosed;
	}

	public void setClosed(boolean isClosed) {
		this.isClosed = isClosed;
	}

	public int getTimeCanTeachID() {
		return timeCanTeachID;
	}

	public void setTimeCanTeachID(int timeCanTeachID) {
		this.timeCanTeachID = timeCanTeachID;
	}

	public String getSubjects() {
		return subjects;
	}

	public void setSubjects(String subjects) {
		this.subjects = subjects;
	}

}

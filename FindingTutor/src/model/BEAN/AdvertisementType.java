/**
 * 
 */
package model.BEAN;

/**
 * @author chinh_huynh
 *
 */
public class AdvertisementType {
	private String advertisementTypeDetail;
	private int advertisementTypeID;

	public AdvertisementType(int advertisementTypeID, String advertisementTypeDetail) {
		super();
		this.advertisementTypeID = advertisementTypeID;
		this.advertisementTypeDetail = advertisementTypeDetail;
	}

	public AdvertisementType() {
		super();
	}

	public String getAdvertisementTypeDetail() {
		return advertisementTypeDetail;
	}

	public void setAdvertisementTypeDetail(String advertisementTypeDetail) {
		this.advertisementTypeDetail = advertisementTypeDetail;
	}

	public int getAdvertisementTypeID() {
		return advertisementTypeID;
	}

	public void setAdvertisementTypeID(int advertisementTypeID) {
		this.advertisementTypeID = advertisementTypeID;
	}

}

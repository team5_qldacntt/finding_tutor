/**
 * 
 */
package model.BEAN;

import java.util.ArrayList;

/**
 * @author chinh_huynh
 *
 */
public class UserAds {
	private int userAdsID;
	private String email;
	private AdvertisementType advertisementType;
	private int quantity;
	public UserAds() {
		super();
		// TODO Auto-generated constructor stub
	}
	public UserAds(int userAdsID, String email, AdvertisementType advertisementTypeID, int quantity) {
		super();
		this.userAdsID = userAdsID;
		this.email = email;
		advertisementType = advertisementTypeID;
		this.quantity = quantity;
	}
	public int getUserAdsID() {
		return userAdsID;
	}
	public void setUserAdsID(int userAdsID) {
		this.userAdsID = userAdsID;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public AdvertisementType getAdvertisementType() {
		return advertisementType;
	}
	public void setAdvertisementType(AdvertisementType advertisementTypeID) {
		advertisementType = advertisementTypeID;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	

}

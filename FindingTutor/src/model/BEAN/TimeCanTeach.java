/**
 * 
 */
package model.BEAN;

/**
 * @author chinh_huynh
 *
 */
public class TimeCanTeach {
	private int timeCanTeachID;
	private String timeCanTeachDetail;

	public TimeCanTeach() {
		super();

	}

	public TimeCanTeach(int timeCanTeachID, String timeCanTeachDetail) {
		super();
		this.timeCanTeachID = timeCanTeachID;
		this.timeCanTeachDetail = timeCanTeachDetail;
	}

	public int getTimeCanTeachID() {
		return timeCanTeachID;
	}

	public void setTimeCanTeachID(int timeCanTeachID) {
		this.timeCanTeachID = timeCanTeachID;
	}

	public String getTimeCanTeachDetail() {
		return timeCanTeachDetail;
	}

	public void setTimeCanTeachDetail(String timeCanTeachDetail) {
		this.timeCanTeachDetail = timeCanTeachDetail;
	}

}

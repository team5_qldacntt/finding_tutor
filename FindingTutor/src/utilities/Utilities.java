/**
 * 
 */
package utilities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import model.BEAN.Post;
import model.BEAN.User;

/**
 * @author Quang Ngo TP
 *
 */
public class Utilities {
	public static String getCurrentDate(){
		Date today=new Date(System.currentTimeMillis());
		SimpleDateFormat timeFormat= new SimpleDateFormat("yyyy-MM-dd");
		return timeFormat.format(today.getTime());
	}
	public static void main(String[] args) {
		System.out.println(getCurrentDate());
	}
	
	public static boolean checkListUser (ArrayList<User> listUsers, String email){
		for (User user : listUsers) {
			if (user.getEmail().equals(email)){
				return true;
			}
		}
		return false;
	}
	
	public boolean checkNullStrings(String... args){
		for (String string : args){
			if (string == null){
				return false;
			}
		}
		return true;
	}
	
	public static boolean checkDuplicatePost(ArrayList<Post> listPost, Post post){
		for (Post p : listPost){
			if (p.getPostID() == post.getPostID()) return false;
		}
		return true;
	}
}
